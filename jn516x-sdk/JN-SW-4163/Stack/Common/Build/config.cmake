
# DESCRIPTION: Selects a configuration makefile based on JENNIC_STACK

# Base location
set(COMPONENTS_BASE_DIR "${SDK_BASE_DIR}/Components")
set(STACK_BASE_DIR "${SDK_BASE_DIR}/Stack")

###############################################################################
# Include path for standard type definitions

#set(CFLAGS "${CFLAGS} -I${COMPONENTS_BASE_DIR}/Common/Include")
include_directories(${COMPONENTS_BASE_DIR}/Common/Include)

###############################################################################
# Linker library paths

#set(LDFLAGS "${LDFLAGS} -L${COMPONENTS_BASE_DIR}/Library")
link_directories(${COMPONENTS_BASE_DIR}/Library)

link_directories(${COMPONENTS_BASE_DIR}/TimerServer/Include)

###############################################################################
# Common ROM based software components

if (JENNIC_MAC MATCHES "MiniMacShim|MAC")
    #set(INCFLAGS "${INCFLAGS} -I${COMPONENTS_BASE_DIR}/AppApi/Include")
    #set(INCFLAGS "${INCFLAGS} -I${COMPONENTS_BASE_DIR}/MAC/Include")
    include_directories(${COMPONENTS_BASE_DIR}/AppApi/Include)
    include_directories(${COMPONENTS_BASE_DIR}/MAC/Include)
endif()

if (JENNIC_MAC MATCHES "MiniMac|MiniMacShim")
    #set(INCFLAGS "${INCFLAGS} -I${COMPONENTS_BASE_DIR}/MiniMac/Include")
    include_directories(${COMPONENTS_BASE_DIR}/MiniMac/Include)
endif()

if (JENNIC_MAC MATCHES "MiniMac|MiniMacShim|MMAC")
    #set(INCFLAGS "${INCFLAGS} -I${COMPONENTS_BASE_DIR}/MMAC/Include")
    include_directories(${COMPONENTS_BASE_DIR}/MMAC/Include)
endif()

#set(INCFLAGS "${INCFLAGS} -I${COMPONENTS_BASE_DIR}/HardwareApi/Include")
#set(INCFLAGS "${INCFLAGS} -I${COMPONENTS_BASE_DIR}/Aes/Include")
#set(INCFLAGS "${INCFLAGS} -I${COMPONENTS_BASE_DIR}/DBG/Include")
include_directories(${COMPONENTS_BASE_DIR}/HardwareApi/Include)
include_directories(${COMPONENTS_BASE_DIR}/Aes/Include)
include_directories(${COMPONENTS_BASE_DIR}/DBG/Include)

# Software debug support
if (DEBUG STREQUAL "SW")
    message(STATUS "Building SW debug version ...")

    set(LDLIBS "${LDLIBS} SWDebug_${JENNIC_CHIP}")

    # Set SWDEBUG_PORT to UART0 or UART1 dependent on connection to serial port on board
    if (DEBUG_PORT STREQUAL "UART0")
        set(CFLAGS "${CFLAGS} -DSWDEBUG_PORT=0")
        message(STATUS "Software Debug will use UART0")
    elseif (DEBUG_PORT STREQUAL "UART1")
        set(CFLAGS "${CFLAGS} -DSWDEBUG_PORT=1")
        message(STATUS "Software Debug will use UART1")
    endif()

    #set(INCFLAGS "${INCFLAGS} -I${COMPONENTS_BASE_DIR}/GDB/Include")
    include_directories(${COMPONENTS_BASE_DIR}/GDB/Include)
endif()

###############################################################################
# Include stack specific definitions

if (NOT DEFINED JENNIC_STACK)
    message(STATUS "JENNIC_STACK is undefined, defaulting to MAC")
    set(JENNIC_STACK "MAC")
endif()

include("${STACK_BASE_DIR}/${JENNIC_STACK}/Build/config_${JENNIC_STACK}.cmake")

###############################################################################
# Setup path for default stack size definition for the selected stack

#set(LDFLAGS "-TApp_Stack_Size.ld ${LDFLAGS} -L${STACK_BASE_DIR}/${JENNIC_STACK}/Build")
link_directories(${STACK_BASE_DIR}/${JENNIC_STACK}/Build)
set(LDFLAGS "${LDFLAGS} -TApp_Stack_Size.ld") 

###############################################################################

