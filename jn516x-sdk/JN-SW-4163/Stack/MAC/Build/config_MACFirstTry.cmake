
# DESCRIPTION: MAC stack configuration
# defines tool, library and header file details

# ROM based software components
include_directories(
    ${COMPONENTS_BASE_DIR}/MAC/Include
    ${COMPONENTS_BASE_DIR}/TimerServer/Include
    ${COMPONENTS_BASE_DIR}/PDM/Include
)

if(JENNIC_CHIP_FAMILY STREQUAL "JN514x")
    include_directories(${COMPONENTS_BASE_DIR}/Random/Include)
endif()

# RAM based software components
if(JENNIC_CHIP_FAMILY STREQUAL "JN514x")
    set(APPLIBS DBG AES_SW PDM)
endif()

if(JENNIC_CHIP_FAMILY STREQUAL "JN513x")
    set(APPLIBS Random)
endif()

foreach(LIB ${APPLIBS})
    include_directories(${COMPONENTS_BASE_DIR}/${LIB}/Include)
endforeach()

if(TRACE EQUAL 1)
    add_compile_definitions(DBG_ENABLE)
    message("Building trace version ...")
endif()
