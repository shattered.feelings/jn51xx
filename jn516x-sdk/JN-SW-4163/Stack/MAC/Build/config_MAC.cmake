
# DESCRIPTION: MAC stack configuration
# defines tool, library and header file details

# ROM based software components
#set(INCFLAGS "${INCFLAGS} -I${COMPONENTS_BASE_DIR}/MAC/Include")
#set(INCFLAGS "${INCFLAGS} -I${COMPONENTS_BASE_DIR}/TimerServer/Include")
#set(INCFLAGS "${INCFLAGS} -I${COMPONENTS_BASE_DIR}/PDM/Include")
include_directories(${COMPONENTS_BASE_DIR}/MAC/Include)
include_directories(${COMPONENTS_BASE_DIR}/TimerServer/Include)
include_directories(${COMPONENTS_BASE_DIR}/PDM/Include)

if(JENNIC_CHIP_FAMILY STREQUAL "JN514x")
    #set(INCFLAGS "${INCFLAGS} -I${COMPONENTS_BASE_DIR}/Random/Include")
    include_directories(${COMPONENTS_BASE_DIR}/Random/Include)
endif()

# RAM based software components
if(JENNIC_CHIP_FAMILY STREQUAL "JN514x")
    set(APPLIBS_DBG "DBG")
    set(APPLIBS_AES "AES_SW")
    set(APPLIBS_PDM "PDM")
endif()
if(JENNIC_CHIP_FAMILY STREQUAL "JN513x")
    set(APPLIBS_RANDOM "Random")
endif()

foreach(APPLIB ${APPLIBS_DBG} ${APPLIBS_AES} ${APPLIBS_PDM} ${APPLIBS_RANDOM})
    #set(INCFLAGS "${INCFLAGS} -I${COMPONENTS_BASE_DIR}/${APPLIB}/Include")
    include_directories(${COMPONENTS_BASE_DIR}/${APPLIB}/Include)
endforeach()

if(TRACE EQUAL 1)
    set(CFLAGS "${CFLAGS} -DDBG_ENABLE")
    message("Building trace version ...")
endif()
