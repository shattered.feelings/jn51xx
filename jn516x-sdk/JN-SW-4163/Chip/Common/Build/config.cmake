###############################################################################
# Toolchain
###############################################################################

#set(TOOL_BASE_DIR ${SDK_BASE_DIR}/Tools)
set(TOOL_BASE_DIR /usr)
#set(TOOL_COMMON_BASE_DIR ${SDK_BASE_DIR}/../Tools)
set(TOOL_COMMON_BASE_DIR /usr)

set(SUBWCREV ${TOOL_BASE_DIR}/TortoiseSVN/bin/subwcrev)

###############################################################################
#
# Set up defaults for stack configuration
# JENNIC_STACK specifies the full stack (MAC only, JenNet-IP, etc.) and
#   determines which set of libraries and include paths are added to the build
# JENNIC_MAC allows selection of the MAC layer:
#   MAC         for the full MAC
#   MiniMac     for the size-optimized variant
#   MiniMacShim for the size-optimized with shim to the old API
#
# Values are normally specified by the application CMake file; the defaults here
# are for legacy builds that pre-date the selection process
#
###############################################################################

set(JENNIC_STACK MAC)
set(JENNIC_MAC MAC)

###############################################################################
# Include the chip or chip family definitions.
# Chip takes precedence over chip family
###############################################################################


if(DEFINED JENNIC_CHIP)
    include(${SDK_BASE_DIR}/Chip/${JENNIC_CHIP}/Build/config_${JENNIC_CHIP}.cmake)
elseif(DEFINED JENNIC_CHIP_FAMILY)
    include(${SDK_BASE_DIR}/Chip/${JENNIC_CHIP_FAMILY}/Build/config_${JENNIC_CHIP_FAMILY}.cmake)
else()
    message(FATAL_ERROR "JENNIC_CHIP or JENNIC_CHIP_FAMILY must be specified")
endif()

###############################################################################
# Define the selected Jennic chip
###############################################################################

add_compile_definitions(
    JENNIC_CHIP=${JENNIC_CHIP}
    JENNIC_CHIP_${JENNIC_CHIP}
    JENNIC_CHIP_FAMILY=${JENNIC_CHIP_FAMILY}
    JENNIC_CHIP_FAMILY_${JENNIC_CHIP_FAMILY}
    JENNIC_STACK_${JENNIC_STACK}
    JENNIC_MAC_${JENNIC_MAC}
)

###############################################################################
# Chip-independent compiler options
###############################################################################

add_compile_options(
    -Wall
    -Wunreachable-code
    -DEMBEDDED
)

###############################################################################
# Compiler Paths
###############################################################################

set(CC gcc)
set(AS as)
set(LD ld)
set(AR ar)
set(NM nm)
set(STRIP strip)
set(SIZE size)
set(OBJCOPY objcopy)
set(OBJDUMP objdump)
set(RANLIB ranlib)

if(CROSS_COMPILE)
    set(CC ${TOOL_COMMON_BASE_DIR}/${TOOLCHAIN_PATH}/bin/${CROSS_COMPILE}-${CC})
    set(AS ${TOOL_COMMON_BASE_DIR}/${TOOLCHAIN_PATH}/bin/${CROSS_COMPILE}-${AS})
    set(LD ${TOOL_COMMON_BASE_DIR}/${TOOLCHAIN_PATH}/bin/${CROSS_COMPILE}-${LD})
    set(AR ${TOOL_COMMON_BASE_DIR}/${TOOLCHAIN_PATH}/bin/${CROSS_COMPILE}-${AR})
    set(NM ${TOOL_COMMON_BASE_DIR}/${TOOLCHAIN_PATH}/bin/${CROSS_COMPILE}-${NM})
    set(STRIP ${TOOL_COMMON_BASE_DIR}/${TOOLCHAIN_PATH}/bin/${CROSS_COMPILE}-${STRIP})
    set(SIZE ${TOOL_COMMON_BASE_DIR}/${TOOLCHAIN_PATH}/bin/${CROSS_COMPILE}-${SIZE})
    set(OBJCOPY ${TOOL_COMMON_BASE_DIR}/${TOOLCHAIN_PATH}/bin/${CROSS_COMPILE}-${OBJCOPY})
    set(OBJDUMP ${TOOL_COMMON_BASE_DIR}/${TOOLCHAIN_PATH}/bin/${CROSS_COMPILE}-${OBJDUMP})
endif()

###############################################################################

