
# DESCRIPTION: Compiler definitions for the BA2 architecture
# This file should only contain architecture specific options.

# Set toolchain path to use ba2 version of the compiler
set(TOOLCHAIN_PATH ba-elf-gcc) #ba-elf-ba2-r36379
set(CROSS_COMPILE ba-elf)


# BA2 architecture compiler flags
set(CFLAGS "${CFLAGS} -march=ba2 -mcpu=jn51xx")
set(CFLAGS "${CFLAGS} -mredzone-size=4 -mbranch-cost=3")

set(LDFLAGS "${LDFLAGS} -march=ba2 -mcpu=jn51xx")
set(LDFLAGS "${LDFLAGS} -mredzone-size=4 -mbranch-cost=3")

# Omit frame pointer by default
set(CFLAGS "${CFLAGS} -fomit-frame-pointer")
set(LDFLAGS "${LDFLAGS} -fomit-frame-pointer")

# Default to optimizing for size
set(CFLAGS "${CFLAGS} -Os")
set(LDFLAGS "${LDFLAGS} -Os")

# Default to smallest possible enums
set(CFLAGS "${CFLAGS} -fshort-enums")
set(LDFLAGS "${LDFLAGS} -fshort-enums")

# Turn on all common warnings
set(CFLAGS "${CFLAGS} -Wall")

# Turn on some additional useful warnings
set(CFLAGS "${CFLAGS} -Wpacked -Wcast-align")

# Output each function and static data in their own sections
set(CFLAGS "${CFLAGS} -fdata-sections -ffunction-sections")


# Debug Support
if(DEBUG STREQUAL "HW")
    set(DISABLE_LTO "1")
    list(APPEND CFLAGS "-g" "-DGDB")
    list(APPEND LDFLAGS "-g")
    # Optimize at level 0 instead of size
    list(REMOVE_ITEM CFLAGS "-Os")
    list(APPEND CFLAGS "-O0")
    list(REMOVE_ITEM LDFLAGS "-Os")
    list(APPEND LDFLAGS "-O0")
    set(HARDWARE_DEBUG_ENABLED "1")
endif()

if(DEBUG STREQUAL "HW_SIZEOPT")
    set(DISABLE_LTO "1")
    list(APPEND CFLAGS "-g" "-DGDB")
    list(APPEND LDFLAGS "-g")
    set(HARDWARE_DEBUG_ENABLED "1")
    message("No optimization enabled with HW debug ...")
endif()

if(HARDWARE_DEBUG_ENABLED EQUAL 1)
    # Set DEBUG_PORT to UART0 or UART1 dependent on connection to serial port on board
    set(CFLAGS "${CFLAGS} -D${DEBUG_PORT}_DEBUG")
    set(CFLAGS "${CFLAGS} -DHWDEBUG") 
    set(BIN_SUFFIX "_hwdbg")
    message("Building HW debug version ...")
endif()

# Link Time Optimization configuration

# Default (unless debugging) is to compile & link using link time optimization,
# but allow it to be disabled by setting DISABLE_LTO=1
if(NOT DISABLE_LTO EQUAL 1)
    set(CFLAGS "${CFLAGS} -flto")
    set(LDFLAGS "${LDFLAGS} -flto")
endif()
