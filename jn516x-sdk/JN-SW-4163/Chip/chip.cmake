# DESCRIPTION:Configuration make include file for JN516x

###############################################################################
# Chip independent compiler options
###############################################################################
add_compile_options(
        -Wall
        -Wunreachable-code
        # Everything needs this
        -DEMBEDDED
)

###############################################################################
# Chip family dependent compiler options
###############################################################################

add_compile_options(
        -march=ba2 -mcpu=jn51xx
        -mredzone-size=4 -mbranch-cost=3
        -fomit-frame-pointer # Default omits frame pointer+        -fshort-enums # Default to smallest possible enums
        -Wall # Turn on all common warnings
        -Wpacked -Wcast-align # Turn on some additional useful warnings
        -fdata-sections -ffunction-sections # Output each function and static data in their own sections
)

# Debug Support
set(DEBUG_PORT "UART0" CACHE STRING "Set DEBUG_PORT to UART0 or UART1 dependant on connection to serial port on board")
set_property(CACHE DEBUG_PORT PROPERTY STRINGS UART0 UART1)
option(DISABLE_LTO "Disable link-time optimizations" OFF)

if ("${CMAKE_BUILD_TYPE}" STREQUAL "Debug")
    add_compile_definitions(
            GDB
            HWDEBUG
            ${DEBUG_PORT}_DEBUG
    )
    set(BIN_SUFFIX _hwdbg)
else ()
    # Link Time Optimisation

    # Default (unless debugging) is to compile & link using link time optimisation,
    # but allow it to be disabled by setting DISABLE_LTO=1
    if (NOT DISABLE_LTO)
        include(CheckIPOSupported)
        check_ipo_supported(RESULT LTO_AVAILABLE OUTPUT output)
        if (LTO_AVAILABLE)
            set(CMAKE_INTERPROCEDURAL_OPTIMIZATION TRUE)
        endif ()
    endif ()
endif ()

###############################################################################
# Chip dependent compiler options
###############################################################################

# Set configuration option for the user to select the target chip
set(JENNIC_CHIP "" CACHE STRING "JENNIC chip name to target build")
set_property(CACHE JENNIC_CHIP PROPERTY STRINGS "" JN5161 JN5164 JN5168 JN5169)

# Check that a valid chip has been selected
if ("${JENNIC_CHIP}" STREQUAL "")
    message(FATAL_ERROR "The target chip name is not defined. Set JENNIC_CHIP to the chip name of your device.")
endif ()

if ("${JENNIC_CHIP}" MATCHES "JN516[0-9]")
else ()
    message(FATAL_ERROR "The JENNIC_CHIP value \"${JENNIC_CHIP}\" is not supported.")
endif ()

# Ensure chip number and family are set
set(JENNIC_CHIP_FAMILY JN516x)
string(SUBSTRING "${JENNIC_CHIP}" 2 -1 JENNIC_CHIP_NUMBER)
message(STATUS "Targeting chip: ${JENNIC_CHIP}")

# Define numeric values for chip type
add_compile_definitions(
        # From common config.mk
        JENNIC_CHIP=${JENNIC_CHIP}
        JENNIC_CHIP_${JENNIC_CHIP}
        JENNIC_CHIP_FAMILY=${JENNIC_CHIP_FAMILY}
        JENNIC_CHIP_FAMILY_NAME=_${JENNIC_CHIP_FAMILY}

        # From individual chip makefiles
        JN516x=5160
        ${JENNIC_CHIP}=${JENNIC_CHIP_NUMBER}
        JENNIC_CHIP_NAME=_${JENNIC_CHIP}
)

# Base dir for the selected chip
set(CHIP_BASE_DIR ${SDK_BASE_DIR}/Chip/${JENNIC_CHIP})


###############################################################################
# Chip feature options
###############################################################################

# Feature set:
#   JENNIC_HW_xx is silicon feature and so fixed
#   JENNIC_SW_xx is build option and can be changed
#
#   JENNIC_HW_BBC_RXINCCA:    BBC RX during CCA capability
#   JENNIC_HW_BBC_DMA:        BBC frame buffer DMA mechanism
#   JENNIC_HW_BBC_ISA:        BBC inline security engine
#   JENNIC_SW_EXTERNAL_FLASH: Whether external flash is supported
#   JN516X_DMA_UART_BACKWARDS_COMPATIBLE_API: API behaves like the older versions with no visible UART DMA
#   UART_BACKWARDS_COMPATIBLE_API: API behaves like the older versions (supercedes above)
#   PDM_DESCRIPTOR_BASED_API: Use standard API interface
#   WATCHDOG_ENABLED: Watchdog is enabled by default on chip, so allow disable if required

add_compile_definitions(
        JENNIC_HW_BBC_RXINCCA=1
        JENNIC_HW_BBC_DMA=1
        JENNIC_HW_BBC_ISA=0
        JENNIC_SW_EXTERNAL_FLASH=0
        JN516X_DMA_UART_BACKWARDS_COMPATIBLE_API=1
        UART_BACKWARDS_COMPATIBLE_API=1
        #PDM_DESCRIPTOR_BASED_API=1
        RXPOWERADJUST_SUPPORT
        WATCHDOG_ENABLED
)

# Chip specific C flags
if ("${JENNIC_CHIP}" STREQUAL "JN5169")
    add_compile_definitions(
            #WIFICM_SUPPORT
            RXPOWERADJUST_SUPPORT
    )
endif ()

# Linker flags
add_link_options(-nostartfiles)

# Library search paths
add_link_options(-L${CHIP_BASE_DIR}/Build)

###############################################################################
# Chip network stack options
###############################################################################

# Set up defaults for stack configuration
# JENNIC_STACK specifies the full stack (MAC only, JenNet-IP, etc.) and
#   determines which set of libraries and include paths are added to the build
# JENNIC_MAC allows selection of the MAC layer:
#   MAC         for full MAC
#   MiniMac     for size-optimised variant
#   MiniMacShim for size-optimised with shim to the old API

set(JENNIC_STACK "MAC" CACHE STRING "JENNIC stack build")
set_property(CACHE JENNIC_STACK PROPERTY STRINGS "None" "MAC" "JIP")

set(JENNIC_MAC "MAC" CACHE STRING "JENNIC MAC build")
set_property(CACHE JENNIC_MAC PROPERTY STRINGS "MAC" "MiniMac" "MiniMacShim")

# JenNet-IP always uses the MiniMac.
if ("${JENNIC_STACK}" STREQUAL "JIP")
    set(JENNIC_MAC MiniMac CACHE FORCE)
endif ()

add_compile_definitions(
        JENNIC_STACK_${JENNIC_STACK}
        JENNIC_MAC_${JENNIC_MAC}
)

# Standard libraries: Peripheral API, MAC, etc.
if (NOT ("${JENNIC_STACK}" STREQUAL "None"))
    if ("${JENNIC_MAC}" STREQUAL MAC)
        list(APPEND LDLIBS AppApi_${REDUCED_MAC_LIB_SUFFIX} ${JENNIC_CHIP})
        list(APPEND LDLIBS MAC_${REDUCED_MAC_LIB_SUFFIX} ${JENNIC_CHIP})
        list(APPEND LDLIBS TimerServer_${JENNIC_CHIP_FAMILY})
        list(APPEND LDLIBS TOF_${JENNIC_CHIP_FAMILY})
        list(APPEND LDLIBS Xcv_${JENNIC_CHIP})
    else ()
        if ("${JENNIC_MAC}" MATCHES "MiniMac|MiniMacShim")
            list(APPEND LDLIBS MiniMac_${JENNIC_CHIP})
            list(APPEND LDLIBS MiniMacShim_${JENNIC_CHIP_FAMILY})
        endif ()
    endif ()
    list(APPEND LDLIBS MMAC_${JENNIC_CHIP})
    list(APPEND LDLIBS JPT_${JENNIC_CHIP})
endif ()

list(APPEND LDLIBS Aes_${JENNIC_CHIP_FAMILY})
list(APPEND LDLIBS HardwareApi_${JENNIC_CHIP})
list(APPEND LDLIBS MicroSpecific_${JENNIC_CHIP_FAMILY})
list(APPEND LDLIBS Boot_${JENNIC_CHIP_FAMILY})

# Stack-specific PDM library variants
if ("${JENNIC_STACK}" STREQUAL JIP)
    list(APPEND LDLIBS PDM_EEPROM_${JENNIC_CHIP_FAMILY})
endif ()
if (NOT ("${JENNIC_STACK}" MATCHES "MAC|None"))
    list(APPEND LDLIBS PDM_EEPROM_${JENNIC_CHIP_FAMILY}_NO_RTOS)
endif ()

# Export linker command file if building the patch library
if ("${JENNIC_STACK}" STREQUAL "None")
    set(LINKER_FILE AppBuildNone)
else ()
    if ("${JENNIC_STACK}" STREQUAL "JIP")
        set(LINKER_FILE AppBuildJip)
    else ()
        if ("${JENNIC_MAC}" MATCHES "MiniMac|MiniMacShim")
            set(LINKER_FILE AppBuildNone)
        else ()
            set(LINKER_FILE AppBuildMac)
        endif ()
    endif ()
endif ()

set(LINKCMD ${LINKER_FILE}.ld) #modify this to not us absolute links(/usr/jn516x-sdk/JN-SW-4163/Chip/JN5169/Build/)

# MAC security enable (Mini MAC with JenNet-IP)
if ("${JENNIC_MAC}" MATCHES "MiniMac|MiniMacShim")
    if ("${JENNIC_STACK}" STREQUAL "JIP")
        add_link_options(-Wl,-ueSecurityTxPrepare -Wl,-ueSecurityTxEncrypt -Wl,-ubSecurityRxProcess)
    endif ()
endif ()

# Hardware debug support (NOTE: JN516x doesn't need a separate library as JTag initialized in bootloader)
if (HARDWARE_DEBUG_ENABLED EQUAL 1)
    add_link_options(-Wl,--defsym,g_bSWConf_Debug=1)
    if (DEBUG_PORT STREQUAL "UART1")
        add_link_options(-Wl,-defsym,g_bSWConf_AltDebugPort=1)
    else ()
        if (NOT (DEBUG_PORT STREQUAL "UART0"))
            message(FATAL_ERROR "If DEBUG=HW: Must define DEBUG_PORT=UART0 or DEBUG_PORT=UART1")
        endif ()
    endif ()
    message("Building HW debug version ...")
endif ()

#Set variables to export
set(JENNIC_CHIP_FAMILY ${JENNIC_CHIP_FAMILY})
set(JENNIC_CHIP ${JENNIC_CHIP})
set(LDLIBS ${LDLIBS})
set(CFLAGS ${CFLAGS})
set(LDFLAGS ${LDFLAGS})
set(LINKCMD ${LINKCMD})
