
# Ensure chip and family are set
set(JENNIC_CHIP_FAMILY JN516x)


# Define numeric value for chip type
set(JN516x "5160")
set(JN5169 "5169")
set(JENNIC_CHIP_NAME "_JN5169")
set(JENNIC_CHIP_FAMILY_NAME "_JN516x")




set(CFLAGS "${CFLAGS} -DJN516x=${JN516x}")
set(CFLAGS "${CFLAGS} -DJN5169=${JN5169}")
set(CFLAGS "${CFLAGS} -DJENNIC_CHIP_NAME=${JENNIC_CHIP_NAME}")
set(CFLAGS "${CFLAGS} -DJENNIC_CHIP_FAMILY_NAME=${JENNIC_CHIP_FAMILY_NAME}")


# Base dir for selected chip
set(CHIP_BASE_DIR ${SDK_BASE_DIR}/Chip/${JENNIC_CHIP})

# include definitions for the BA2 architecture
include(${SDK_BASE_DIR}/Chip/Common/Build/config_ba2.cmake)

# Chip specific C flags
# watchdog is enabled by default on chip so allow disable if reqd
set(CFLAGS "${CFLAGS} -DWATCHDOG_ENABLED")


# Feature set:
#   JENNIC_HW_xx is silicon feature and so fixed
#   JENNIC_SW_xx is build option and can be changed
#
#   JENNIC_HW_BBC_RXINCCA:    BBC RX during CCA capability
#   JENNIC_HW_BBC_DMA:        BBC frame buffer DMA mechanism
#   JENNIC_HW_BBC_ISA:        BBC inline security engine 
#   JENNIC_SW_EXTERNAL_FLASH: whether external flash is supported
#   JN516X_DMA_UART_BACKWARDS_COMPATIBLE_API: API behaves like the older versions with no visible UART DMA 
#   UART_BACKWARDS_COMPATIBLE_API: API behaves like the older versions (supercedes above)
#   PDM_DESCRIPTOR_BASED_API: Use standard API interface 


set(CFLAGS "${CFLAGS} -DJENNIC_HW_BBC_RXINCCA=1")
set(CFLAGS "${CFLAGS} -DJENNIC_HW_BBC_DMA=1")
set(CFLAGS "${CFLAGS} -DJENNIC_HW_BBC_ISA=0")
set(CFLAGS "${CFLAGS} -DJENNIC_SW_EXTERNAL_FLASH=0")
set(CFLAGS "${CFLAGS} -DJN516X_DMA_UART_BACKWARDS_COMPATIBLE_API=1")
set(CFLAGS "${CFLAGS} -DDUART_BACKWARDS_COMPATIBLE_API=1")
    #"-DPDM_DESCRIPTOR_BASED_API=1"
    #"-DWIFICM_SUPPORT"
set(CFLAGS "${CFLAGS} -DRXPOWERADJUST_SUPPORT")


# Linker flags
set(LDFLAGS -nostartfiles)

# Library search paths
#set(LDFLAGS "${LDFLAGS} -L${CHIP_BASE_DIR}/Build")
#set(LDFLAGS "${LDFLAGS} -L${CHIP_BASE_DIR}/Library")
link_directories(
    ${CHIP_BASE_DIR}/Build
    ${CHIP_BASE_DIR}/Library
)

# JenNet-IP always uses the MiniMac.
if(JENNIC_STACK STREQUAL JIP)
    set(JENNIC_MAC MiniMac)
endif()

# Standard libraries: Peripheral API, MAC, etc.
if(NOT (JENNIC_STACK STREQUAL None))
    if(JENNIC_MAC STREQUAL MAC)
        #list(APPEND LDLIBS "-lAppApi_${REDUCED_MAC_LIB_SUFFIX}${JENNIC_CHIP}")
        #list(APPEND LDLIBS "-lMAC_${REDUCED_MAC_LIB_SUFFIX}${JENNIC_CHIP}")
        #list(APPEND LDLIBS "-lTimerServer_${JENNIC_CHIP_FAMILY}")
        #list(APPEND LDLIBS "-lTOF_${JENNIC_CHIP_FAMILY}")
        #list(APPEND LDLIBS "-lXcv_${JENNIC_CHIP}")
        list(APPEND LDLIBS AppApi_${REDUCED_MAC_LIB_SUFFIX}${JENNIC_CHIP})
        list(APPEND LDLIBS MAC_${REDUCED_MAC_LIB_SUFFIX}${JENNIC_CHIP})
        list(APPEND LDLIBS TimerServer_${JENNIC_CHIP_FAMILY})
        list(APPEND LDLIBS TOF_${JENNIC_CHIP_FAMILY})
        list(APPEND LDLIBS Xcv_${JENNIC_CHIP}) 
 
    else()
        if(JENNIC_MAC MATCHES "MiniMac|MiniMacShim")
            #list(APPEND LDLIBS "-lMiniMac_${JENNIC_CHIP}")
            #list(APPEND LDLIBS "-lMiniMacShim_${JENNIC_CHIP_FAMILY}")
            list(APPEND LDLIBS MiniMac_${JENNIC_CHIP})
            list(APPEND LDLIBS MiniMacShim_${JENNIC_CHIP_FAMILY})
        endif()
    endif()
    #list(APPEND LDLIBS "-lMMAC_${JENNIC_CHIP}")
    #list(APPEND LDLIBS "-lJPT_${JENNIC_CHIP}")
    list(APPEND LDLIBS MMAC_${JENNIC_CHIP})
    list(APPEND LDLIBS JPT_${JENNIC_CHIP})
endif()

list(APPEND LDLIBS Aes_${JENNIC_CHIP_FAMILY})
list(APPEND LDLIBS HardwareApi_${JENNIC_CHIP})
list(APPEND LDLIBS MicroSpecific_${JENNIC_CHIP_FAMILY})
list(APPEND LDLIBS Boot_${JENNIC_CHIP_FAMILY})
#list(APPEND LDLIBS "-lAes_${JENNIC_CHIP_FAMILY}")
#list(APPEND LDLIBS "-lHardwareApi_${JENNIC_CHIP}")
#list(APPEND LDLIBS "-lMicroSpecific_${JENNIC_CHIP_FAMILY}")
#list(APPEND LDLIBS "-lBoot_${JENNIC_CHIP_FAMILY}")

# Stack-specific PDM library variants
if(JENNIC_STACK STREQUAL JIP)
    list(APPEND LDLIBS "-lPDM_EEPROM_${JENNIC_CHIP_FAMILY}")
endif()

if(NOT (JENNIC_STACK MATCHES "MAC|None"))
    list(APPEND LDLIBS PDM_EEPROM_${JENNIC_CHIP_FAMILY}_NO_RTOS)
    # list(APPEND LDLIBS "-lPDM_EEPROM_${JENNIC_CHIP_FAMILY}_NO_RTOS")
endif()

# Export linker command file if building patch library
if(JENNIC_STACK STREQUAL "None")
    set(LINKER_FILE "AppBuildNone")
elseif(JENNIC_STACK STREQUAL "JIP")
    set(LINKER_FILE "AppBuildJip")
elseif(JENNIC_MAC MATCHES "MiniMac|MiniMacShim")
    set(LINKER_FILE "AppBuildNone")
else()
    set(LINKER_FILE "AppBuildMac")
endif()

set(LINKCMD ${LINKER_FILE}.ld)

# MAC security enable (Mini MAC with JenNet-IP)
if(JENNIC_MAC MATCHES "MiniMac|MiniMacShim")
    if(JENNIC_STACK STREQUAL JIP)
        set(LDFLAGS "${LDFLAGS} -Wl,-ueSecurityTxPrepare -Wl,-ueSecurityTxEncrypt -Wl,-ubSecurityRxProcess")
    endif()
endif()

# Hardware debug support (NOTE: JN516x doesn't need separate library as JTag initialised in bootloader)
if(HARDWARE_DEBUG_ENABLED EQUAL 1)
    #set(LDFLAGS "${LDFLAGS} -Wl,--defsym,g_bSWConf_Debug=1")
    list(APPEND LDLIBS "-Wl,--defsym,g_bSWConf_Debug=1")
    if(DEBUG_PORT STREQUAL "UART1")
        #set(LDFLAGS "${LDFLAGS} -Wl,-defsym,g_bSWConf_AltDebugPort=1")
        list(APPEND LDLIBS "-Wl,-defsym,g_bSWConf_AltDebugPort=1")
    else()
        if(NOT (DEBUG_PORT STREQUAL "UART0"))
            message(FATAL_ERROR "If DEBUG=HW: Must define DEBUG_PORT=UART0 or DEBUG_PORT=UART1")
        endif()
    endif()
    message("Building HW debug version ...")
endif()

# Set variables to export
#set(JENNIC_CHIP_FAMILY ${JENNIC_CHIP_FAMILY})
#set(JENNIC_CHIP ${JENNIC_CHIP})
#set(LDLIBS ${LDLIBS})
#set(CFLAGS ${CFLAGS})
#set(LDFLAGS ${LDFLAGS})
#set(LINKCMD ${LINKCMD})

# exports
#export(JENNIC_CHIP_FAMILY JENNIC_CHIP LDLIBS CFLAGS LDFLAGS LINKCMD)

