/*****************************************************************************
 *
 * MODULE:             Flash Index Sector API
 *
 * DESCRIPTION:        Functions for interacting with the index sector
 *
 ****************************************************************************
 *
 * This software is owned by NXP B.V. and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on NXP products [NXP Microcontrollers such as JN5148, JN5142, JN5139].
 * You, and any third parties must reproduce the copyright and warranty notice
 * and any other legend of ownership on each copy or partial copy of the
 * software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright NXP B.V. 2012. All rights reserved
 *
 ***************************************************************************/

#ifndef FLASH_INDEX_API_6X_INCLUDE_H_INCLUDED
#define FLASH_INDEX_API_6X_INCLUDE_H_INCLUDED

/****************************************************************************/
/***        Include Files                                                 ***/
/****************************************************************************/

#include "jendefs.h"

#if defined __cplusplus
extern "C" {
#endif

/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/
#define VBO_VOLTAGE_1_95V                               (0)
#define VBO_VOLTAGE_2_00V                               (1)
#define VBO_VOLTAGE_2_10V                               (2)
#define VBO_VOLTAGE_2_20V                               (3)
#define VBO_VOLTAGE_2_30V                               (4)
#define VBO_VOLTAGE_2_40V                               (5)
#define VBO_VOLTAGE_2_70V                               (6)
#define VBO_VOLTAGE_3_00V                               (7)

#define CRP_LEVEL_0_NO_PROTECTION                       (3)
#define CRP_LEVEL_1_FLASH_READ_PROTECTION               (1)
#define CRP_LEVEL_2_NO_PROGRAMMING_ALLOWED_VIA_UART     (2)


/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/


/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/
PUBLIC bool_t bAHI_WriteCustomerMACID(
        uint64      u64CustomerMACId);

PUBLIC bool_t bAHI_WriteCustomerAESkey(
        uint32      au32AESkey[4]);

PUBLIC bool_t bAHI_WriteUserData(
        uint8       u8UserFieldIndex,
        uint32      au32userData[4]);

PUBLIC bool_t bAHI_WriteCustomerSettings(
        bool_t      bJTAGdisable,
        uint8       u8VBOthreshold,
        uint8       u8CRP,
        bool_t      bEncryptedExternalFlash,
        bool_t      bDisableLoadFromExternalFlash);

PUBLIC bool_t bAHI_ReadMACID(
        bool_t      bReadCustomerMACID,
        uint64     *pu64CustomerMACId);

PUBLIC bool_t bAHI_ReadCustomerAESkey(
        uint32      au32AESkey[4]);

PUBLIC bool_t bAHI_ReadUserData(
        uint8       u8UserFieldIndex,
        uint32      au32userData[4]);

PUBLIC bool_t bAHI_ReadCustomerSettings(
        bool_t     *pbJTAGdisable,
        uint8      *pu8VBOthreshold,
        uint8      *pu8CRP,
        bool_t     *pbEncryptedExternalFlash,
        bool_t     *pbDisableLoadFromExternalFlash);

/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/


#if defined __cplusplus
};
#endif

#endif /* FLASH_INDEX_API_6X_INCLUDE_H_INCLUDED */

/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
