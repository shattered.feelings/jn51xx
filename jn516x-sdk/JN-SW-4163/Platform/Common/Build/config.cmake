# CMake version
#cmake_minimum_required(VERSION 3.22)

# Include the selected PCB definition
if(JENNIC_PCB)
    if(JENNIC_PCB STREQUAL "DEVKIT1")
        set(PLATFORM_BASE_DIR ${SDK_BASE_DIR}/Platform/DK1)
    elseif(JENNIC_PCB STREQUAL "DEVKIT2")
        set(PLATFORM_BASE_DIR ${SDK_BASE_DIR}/Platform/DK2)
    elseif(JENNIC_PCB STREQUAL "HPDEVKIT")
        set(PLATFORM_BASE_DIR ${SDK_BASE_DIR}/Platform/HPDevKit)
    elseif(JENNIC_PCB STREQUAL "NTS")
        set(PLATFORM_BASE_DIR ${SDK_BASE_DIR}/Platform/NTS)
    elseif(JENNIC_PCB STREQUAL "DEVKIT3")
        set(PLATFORM_BASE_DIR ${SDK_BASE_DIR}/Platform/DK3)
    elseif(JENNIC_PCB STREQUAL "DEVKIT4")
        set(PLATFORM_BASE_DIR ${SDK_BASE_DIR}/Platform/DK4)
    else()
        message(FATAL_ERROR "JENNIC_PCB is not defined. Define for the Jennic development kit you are using.")
    endif()

    # Compiler flags
    include_directories(${SDK_BASE_DIR}/Platform/Common/Include)

    # Define the selected Jennic platform
    add_compile_definitions(
        JENNIC_PCB=${JENNIC_PCB}
        JENNIC_PCB_${JENNIC_PCB}
    )

    # Include platform-specific definitions
    include(${PLATFORM_BASE_DIR}/Build/PlatformConfig.cmake)
endif()
