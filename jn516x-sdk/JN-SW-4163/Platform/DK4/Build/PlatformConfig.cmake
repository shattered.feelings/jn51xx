
# MODULE:   PlatformConfig.mk
# DESCRIPTION: DK4 Platform specific definitions

# Include directories for the compiler
include_directories(${PLATFORM_BASE_DIR}/Include)

# Linker flags
link_directories(${PLATFORM_BASE_DIR}/Library)
link_libraries(BoardLib_${JENNIC_CHIP_FAMILY})
