###############################################################################
# Base directory of sdk2 layout i.e c:\Jennic
###############################################################################

set(SDK_BASE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/../../..)

# DK4 remote board only
set(BOARDDK4_BASE ${SDK_BASE_DIR}/Platform/DK4)
set(BOARDCOMMON_BASE ${SDK_BASE_DIR}/Platform)

# Export variables
#set(SDK_BASE_DIR ${SDK_BASE_DIR} PARENT_SCOPE)
#set(BOARDDK4_BASE ${BOARDDK4_BASE} PARENT_SCOPE)
#set(BOARDCOMMON_BASE ${BOARDCOMMON_BASE} PARENT_SCOPE)
#set(JENNIC_PCB ${JENNIC_PCB} PARENT_SCOPE)
#set(JENNIC_CHIP ${JENNIC_CHIP} PARENT_SCOPE)

include(${SDK_BASE_DIR}/Chip/Common/Build/config.cmake)

###############################################################################
# Targets
###############################################################################

# Build target
add_custom_target(all
    COMMAND ${CMAKE_MAKE_PROGRAM} -C ${BOARDDK4_BASE}/Build -f BoardLib_${JENNIC_CHIP_FAMILY}.cmake
    COMMENT "Building All Board Libraries"
)

# Clean target
add_custom_target(clean_all
    COMMAND ${CMAKE_MAKE_PROGRAM} -C ${BOARDDK4_BASE}/Build -f BoardLib_${JENNIC_CHIP_FAMILY}.cmake clean
    COMMENT "Cleaning All Board Libraries"
)
