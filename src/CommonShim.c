/*
 * Copyright (c) 2024 Tokushima University, Japan.
 *
 * SPDX-License-Identifier: GPL-2.0-only
 *
 * Author:
 *  Alberto Gallegos Ramonet <alramonet@is.tokushima-u.ac.jp>
 */


#include "CommonShim.h"

void
AppWarmStart()
{
    AppColdStart();
}

void
AppColdStart()
{
// Disable watchdog if enabled by default
#ifdef WATCHDOG_ENABLED
    vAHI_WatchdogStop();
#endif

    InitializeSystem();
    // vAHI_DioSetDirection (0,E_AHI_DIO1_INT);
    // vAHI_DioSetOutput (0,E_AHI_DIO1_INT);
    DBG_vUartInit(DBG_E_UART_0, DBG_E_UART_BAUD_RATE_115200);

    vAHI_Uart0RegisterCallback(&ReceiveByte);
    // vAHI_Uart1RegisterCallback(&ReceiveByte);
    vAHI_UartSetInterrupt(DBG_E_UART_0,
                          FALSE,
                          FALSE,                    // Enable Rx line status
                          FALSE,                    // Enable Tx FIFO empty
                          TRUE,                     // Enable Rx Data
                          E_AHI_UART_FIFO_LEVEL_1); // Number of bits to wait in the Rx FIFO before
                                                    // triggering the interrupt (1,8,14)

    m_state = START;

    while (1)
    {
        ProcessEventQueues();
    }
}

void
InitializeSystem()
{
    /* Setup interface to MAC */
    (void)u32AHI_Init();
    (void)u32AppQApiInit(NULL, NULL, NULL);

    /* Initialise end device state */
    sEndDeviceData.eState = E_STATE_IDLE;
    sEndDeviceData.u8TxPacketSeqNb = 0;
    sEndDeviceData.u8RxPacketSeqNb = 0;

    // Set up the MAC handles. Must be called AFTER u32AppQApiInit()
    s_pvMac = pvAppApiGetMacHandle();
    s_psMacPib = MAC_psPibGetHandle(s_pvMac);

    // Set Pan ID in PIB (also sets match register in hardware)
    // MAC_vPibSetPanId(s_pvMac, PAN_ID);

    // Allow nodes to associate, this should be set externally with MLME-SET
    s_psMacPib->bAssociationPermit = 1;

    // No Csma Backoff chances
    // MAC_vPibSetMaxCsmaBackoffs(s_pvMac,0);
    // No Random BackOff Periods
    // MAC_vPibSetMinBe(s_pvMac,1);

    // Enable receiver to be on when idle
    MAC_vPibSetRxOnWhenIdle(s_pvMac, TRUE, FALSE);
}

void
ProcessEventQueues()
{
    MAC_MlmeDcfmInd_s* psMlmeInd;
    MAC_McpsDcfmInd_s* psMcpsInd;
    AppQApiHwInd_s* psAHI_Ind;

    // Check for anything on the MCPS upward queue
    do
    {
        psMcpsInd = psAppQApiReadMcpsInd();
        if (psMcpsInd != NULL)
        {
            ProcessIncomingMcps(psMcpsInd);
            vAppQApiReturnMcpsIndBuffer(psMcpsInd);
        }
    } while (psMcpsInd != NULL);

    // Check for anything on the MLME upward queue
    do
    {
        psMlmeInd = psAppQApiReadMlmeInd();
        if (psMlmeInd != NULL)
        {
            ProcessIncomingMlme(psMlmeInd);
            vAppQApiReturnMlmeIndBuffer(psMlmeInd);
        }
    } while (psMlmeInd != NULL);

    // Check for anything on the AHI upward queue
    do
    {
        psAHI_Ind = psAppQApiReadHwInd();
        if (psAHI_Ind != NULL)
        {
            ProcessIncomingHwEvent(psAHI_Ind);
            vAppQApiReturnHwIndBuffer(psAHI_Ind);
        }
    } while (psAHI_Ind != NULL);
}

void
ProcessIncomingHwEvent(AppQApiHwInd_s* psAHI_Ind)
{
    // TODO: respond to any hardware events here
}

void
AssociateConfirm(MAC_MlmeDcfmInd_s* psMlmeInd)
{
    uint8_t pos = 0;
    uint8_t associateCfmParams[6];
    // DBG_vPrintf(TRUE,"InsideAssocCFM: %x %d",
    // psMlmeInd->uParam.sDcfmAssociate.u16AssocShortAddr,psMlmeInd->uParam.sDcfmAssociate.u8Status);
    associateCfmParams[pos++] = 0xAA;
    associateCfmParams[pos++] = ASSOCIATE_CFM;
    // Parameter Size = 3
    // Status (1) + AssocShortAddr(2)
    associateCfmParams[pos++] = 3;
    associateCfmParams[pos++] = psMlmeInd->uParam.sDcfmAssociate.u8Status;
    associateCfmParams[pos++] = (psMlmeInd->uParam.sDcfmAssociate.u16AssocShortAddr) & 0xFF;
    associateCfmParams[pos++] = (psMlmeInd->uParam.sDcfmAssociate.u16AssocShortAddr >> 8) & 0xFF;

    // TODO: Add security information here when supported

    SendBytes(associateCfmParams, 6);
}

void
AssociateIndication(MAC_MlmeDcfmInd_s* psMlmeInd)
{
    uint8_t associateIndParams[12];
    uint16_t pos = 0;

    uint32_t addr32H = psMlmeInd->uParam.sIndAssociate.sDeviceAddr.u32H;
    uint32_t addr32L = psMlmeInd->uParam.sIndAssociate.sDeviceAddr.u32L;
    uint8_t capabilityInfo = psMlmeInd->uParam.sIndAssociate.u8Capability;

    associateIndParams[pos++] = 0xAA;
    associateIndParams[pos++] = ASSOCIATE_IND;
    associateIndParams[pos++] = 9; // total size of the parameters in bytes.

    associateIndParams[pos++] = (addr32L & 0xFF);
    associateIndParams[pos++] = (addr32L >> 8) & 0xFF;
    associateIndParams[pos++] = (addr32L >> 16) & 0xFF;
    associateIndParams[pos++] = (addr32L >> 24) & 0xFF;

    associateIndParams[pos++] = (addr32H & 0xFF);
    associateIndParams[pos++] = (addr32H >> 8) & 0xFF;
    associateIndParams[pos++] = (addr32H >> 16) & 0xFF;
    associateIndParams[pos++] = (addr32H >> 24) & 0xFF;

    associateIndParams[pos++] = capabilityInfo;

    // DBG_vPrintf(TRUE,"values %x %x %x %x",associateIndParams[3],
    // associateIndParams[4],associateIndParams[5],associateIndParams[6]);

    // TODO:
    // Note: LQI information not available in this primitive,
    // also, security information is not pushed up the stack.

    SendBytes(associateIndParams, 12);
}

void
SyncLossIndication(MAC_MlmeDcfmInd_s* psMlmeInd)
{
    DBG_vPrintf(TRUE,
                "\n***ERROR Syncronization with Beacon Lost| reason %x ***\n",
                psMlmeInd->uParam.sIndSyncLoss.u8Reason);
}

void
CommStatusIndication(MAC_MlmeDcfmInd_s* psMlmeInd)
{
    uint8_t commStatusIndParams[24];
    uint16_t pos = 0;
    uint8_t paramSize = 5;

    uint16_t panId = psMlmeInd->uParam.sIndCommStatus.sSrcAddr.u16PanId;
    uint8_t srcAddrMode = psMlmeInd->uParam.sIndCommStatus.sSrcAddr.u8AddrMode;
    uint8_t dstAddrMode = psMlmeInd->uParam.sIndCommStatus.sDstAddr.u8AddrMode;

    if (srcAddrMode == SHORT_ADDR)
    {
        paramSize += 8;
    }
    else
    {
        paramSize += 2;
    }

    if (dstAddrMode == SHORT_ADDR)
    {
        paramSize += 8;
    }
    else
    {
        paramSize += 2;
    }

    commStatusIndParams[pos++] = 0xAA;
    commStatusIndParams[pos++] = COMM_STATUS_IND;
    // Parameter Size = 5 + SrcAddress size  + DstAddrSize
    // PanId (2) + SrcAddrMode(1) + DstAddrMode(1) + Status (1) + srcAddr(variable) +
    // dstAddr(variable)
    commStatusIndParams[pos++] = paramSize; // total size of the parameters in bytes.
    commStatusIndParams[pos++] = (panId & 0xFF);
    commStatusIndParams[pos++] = (panId >> 8) & 0xFF;
    commStatusIndParams[pos++] = srcAddrMode;

    if (srcAddrMode == EXT_ADDR)
    {
        uint32_t srcAddr32L = psMlmeInd->uParam.sIndCommStatus.sSrcAddr.uAddr.sExt.u32L;
        uint32_t srcAddr32H = psMlmeInd->uParam.sIndCommStatus.sSrcAddr.uAddr.sExt.u32H;
        commStatusIndParams[pos++] = (srcAddr32L & 0xFF);
        commStatusIndParams[pos++] = (srcAddr32L >> 8) & 0xFF;
        commStatusIndParams[pos++] = (srcAddr32L >> 16) & 0xFF;
        commStatusIndParams[pos++] = (srcAddr32L >> 24) & 0xFF;

        commStatusIndParams[pos++] = (srcAddr32H & 0xFF);
        commStatusIndParams[pos++] = (srcAddr32H >> 8) & 0xFF;
        commStatusIndParams[pos++] = (srcAddr32H >> 16) & 0xFF;
        commStatusIndParams[pos++] = (srcAddr32H >> 24) & 0xFF;
    }
    else
    {
        uint16_t srcAddr = psMlmeInd->uParam.sIndCommStatus.sSrcAddr.uAddr.u16Short;
        commStatusIndParams[pos++] = (srcAddr & 0xFF);
        commStatusIndParams[pos++] = (srcAddr >> 8) & 0xFF;
    }

    commStatusIndParams[pos++] = dstAddrMode;

    if (dstAddrMode == EXT_ADDR)
    {
        uint32_t dstAddr32L = psMlmeInd->uParam.sIndCommStatus.sDstAddr.uAddr.sExt.u32L;
        uint32_t dstAddr32H = psMlmeInd->uParam.sIndCommStatus.sDstAddr.uAddr.sExt.u32H;
        commStatusIndParams[pos++] = (dstAddr32L & 0xFF);
        commStatusIndParams[pos++] = (dstAddr32L >> 8) & 0xFF;
        commStatusIndParams[pos++] = (dstAddr32L >> 16) & 0xFF;
        commStatusIndParams[pos++] = (dstAddr32L >> 24) & 0xFF;

        commStatusIndParams[pos++] = (dstAddr32H & 0xFF);
        commStatusIndParams[pos++] = (dstAddr32H >> 8) & 0xFF;
        commStatusIndParams[pos++] = (dstAddr32H >> 16) & 0xFF;
        commStatusIndParams[pos++] = (dstAddr32H >> 24) & 0xFF;
    }
    else
    {
        uint16_t dstAddr = psMlmeInd->uParam.sIndCommStatus.sDstAddr.uAddr.u16Short;
        commStatusIndParams[pos++] = (dstAddr & 0xFF);
        commStatusIndParams[pos++] = (dstAddr >> 8) & 0xFF;
    }

    commStatusIndParams[pos++] = psMlmeInd->uParam.sIndCommStatus.u8Status;
    // TODO: Add security information when supported

    SendBytes(commStatusIndParams, paramSize + 3);
}

void
ScanConfirm(MAC_MlmeDcfmInd_s* psMlmeInd)
{
    uint8_t paramsSize;
    // Check the total size to transmit through UART

    // Fixed Size params
    // Initial 0xAA byte (1) + Primitive Type (1) + ParamSize(1) + Status (1) + ScanType (1)
    // UnscannedChannels (4) + ResultListSize (1) = 10 bytes
    paramsSize = 10;

    // Variable Size params
    if (psMlmeInd->uParam.sDcfmScan.u8Status == MAC_ENUM_SUCCESS)
    {
        switch (psMlmeInd->uParam.sDcfmScan.u8ScanType)
        {
        case MAC_MLME_SCAN_TYPE_ENERGY_DETECT:
            // Energy Scan values (0-16 bytes)
            paramsSize += psMlmeInd->uParam.sDcfmScan.u8ResultListSize;
            break;
        case MAC_MLME_SCAN_TYPE_ORPHAN:
            break;
        case MAC_MLME_SCAN_TYPE_ACTIVE:
        case MAC_MLME_SCAN_TYPE_PASSIVE: {
            int j = 0;
            // Size of the PAN descriptor elements
            paramsSize = 9;
            for (j = 0; j < psMlmeInd->uParam.sDcfmScan.u8ResultListSize; j++)
            {
                // CoordAddrMode(1) + PanId(2) + Superframe (2) + Timestamp (4)
                // GtsPermit(1) + LogicalCh(1) + LogicalPage(1) + LQI(1) =  13 bytes
                paramsSize += 13;

                if (psMlmeInd->uParam.sDcfmScan.uList.asPanDescr[j].sCoord.u8AddrMode == 2)
                {
                    // Coordinator short address (2 bytes)
                    paramsSize += 2;
                }
                else
                {
                    // Coordinator extended address (8 bytes)
                    paramsSize += 8;
                }
            }
        }
        break;
        }
    }

    uint8_t scanParams[paramsSize];
    uint16_t pos = 0;

    scanParams[pos++] = 0xAA;
    scanParams[pos++] = SCAN_CFM; // ConfirmIndicationType(MAC_MLME_DCFM_SCAN);
    // The size in bytes of the parameters (excluding 0xAA byte, the primitive type, and this value)
    scanParams[pos++] = paramsSize - 3;
    scanParams[pos++] = psMlmeInd->uParam.sDcfmScan.u8Status;
    scanParams[pos++] = psMlmeInd->uParam.sDcfmScan.u8ScanType;
    scanParams[pos++] = (psMlmeInd->uParam.sDcfmScan.u32UnscannedChannels) & 0xFF;
    scanParams[pos++] = (psMlmeInd->uParam.sDcfmScan.u32UnscannedChannels >> 8) & 0xFF;
    scanParams[pos++] = (psMlmeInd->uParam.sDcfmScan.u32UnscannedChannels >> 16) & 0xFF;
    scanParams[pos++] = (psMlmeInd->uParam.sDcfmScan.u32UnscannedChannels >> 24) & 0xFF;
    scanParams[pos++] = psMlmeInd->uParam.sDcfmScan.u8ResultListSize;

    if (psMlmeInd->uParam.sDcfmScan.u8Status == MAC_ENUM_SUCCESS)
    {
        if (psMlmeInd->uParam.sDcfmScan.u8ScanType == MAC_MLME_SCAN_TYPE_ENERGY_DETECT)
        {
            uint8_t i;
            for (i = 0; i < psMlmeInd->uParam.sDcfmScan.u8ResultListSize; i++)
            {
                scanParams[pos++] = psMlmeInd->uParam.sDcfmScan.uList.au8EnergyDetect[i];
            }
        }
        else if (psMlmeInd->uParam.sDcfmScan.u8ScanType != MAC_MLME_SCAN_TYPE_ORPHAN)
        {
            // Add the information from PAN descriptors
            int j = 0;
            for (j = 0; j < psMlmeInd->uParam.sDcfmScan.u8ResultListSize; j++)
            {
                // Coordinator address mode
                scanParams[pos++] =
                    psMlmeInd->uParam.sDcfmScan.uList.asPanDescr[j].sCoord.u8AddrMode;
                // Coordinator PAN ID
                scanParams[pos++] =
                    (psMlmeInd->uParam.sDcfmScan.uList.asPanDescr[j].sCoord.u16PanId) & 0xFF;
                scanParams[pos++] =
                    (psMlmeInd->uParam.sDcfmScan.uList.asPanDescr[j].sCoord.u16PanId >> 8) & 0xFF;

                if (psMlmeInd->uParam.sDcfmScan.uList.asPanDescr[j].sCoord.u8AddrMode == SHORT_ADDR)
                {
                    // Coordinator short address
                    scanParams[pos++] =
                        (psMlmeInd->uParam.sDcfmScan.uList.asPanDescr[j].sCoord.uAddr.u16Short) &&
                        0xFF;
                    scanParams[pos++] =
                        (psMlmeInd->uParam.sDcfmScan.uList.asPanDescr[j].sCoord.uAddr.u16Short >>
                         8) &&
                        0xFF;
                }
                else
                {
                    // Coordinator extended address
                    scanParams[pos++] =
                        (psMlmeInd->uParam.sDcfmScan.uList.asPanDescr[j].sCoord.uAddr.sExt.u32H) &&
                        0xFF;
                    scanParams[pos++] =
                        (psMlmeInd->uParam.sDcfmScan.uList.asPanDescr[j].sCoord.uAddr.sExt.u32H >>
                         8) &&
                        0xFF;
                    scanParams[pos++] =
                        (psMlmeInd->uParam.sDcfmScan.uList.asPanDescr[j].sCoord.uAddr.sExt.u32H >>
                         16) &&
                        0xFF;
                    scanParams[pos++] =
                        (psMlmeInd->uParam.sDcfmScan.uList.asPanDescr[j].sCoord.uAddr.sExt.u32H >>
                         24) &&
                        0xFF;

                    scanParams[pos++] =
                        (psMlmeInd->uParam.sDcfmScan.uList.asPanDescr[j].sCoord.uAddr.sExt.u32L) &&
                        0xFF;
                    scanParams[pos++] =
                        (psMlmeInd->uParam.sDcfmScan.uList.asPanDescr[j].sCoord.uAddr.sExt.u32L >>
                         8) &&
                        0xFF;
                    scanParams[pos++] =
                        (psMlmeInd->uParam.sDcfmScan.uList.asPanDescr[j].sCoord.uAddr.sExt.u32L >>
                         16) &&
                        0xFF;
                    scanParams[pos++] =
                        (psMlmeInd->uParam.sDcfmScan.uList.asPanDescr[j].sCoord.uAddr.sExt.u32L >>
                         24) &&
                        0xFF;
                }

                // Logical Channel
                scanParams[pos++] = psMlmeInd->uParam.sDcfmScan.uList.asPanDescr[j].u8LogicalChan;
                // Logical Page
                scanParams[pos++] = 0; // This device only support Page 0
                // Superframe Specification
                scanParams[pos++] =
                    (psMlmeInd->uParam.sDcfmScan.uList.asPanDescr[j].u16SuperframeSpec) & 0xFF;
                scanParams[pos++] =
                    (psMlmeInd->uParam.sDcfmScan.uList.asPanDescr[j].u16SuperframeSpec >> 8) & 0xFF;
                // GTS permit
                scanParams[pos++] = psMlmeInd->uParam.sDcfmScan.uList.asPanDescr[j].u8GtsPermit;
                // Link Quality Indicator (LQI)
                scanParams[pos++] = psMlmeInd->uParam.sDcfmScan.uList.asPanDescr[j].u8LinkQuality;
                // Time Stamp (32 bits)
                scanParams[pos++] =
                    (psMlmeInd->uParam.sDcfmScan.uList.asPanDescr[j].u32TimeStamp) & 0xFF;
                scanParams[pos++] =
                    (psMlmeInd->uParam.sDcfmScan.uList.asPanDescr[j].u32TimeStamp >> 8) & 0xFF;
                scanParams[pos++] =
                    (psMlmeInd->uParam.sDcfmScan.uList.asPanDescr[j].u32TimeStamp >> 16) & 0xFF;
                scanParams[pos++] =
                    (psMlmeInd->uParam.sDcfmScan.uList.asPanDescr[j].u32TimeStamp >> 24) & 0xFF;

                // scanParams[pos++] =
                // psMlmeInd->uParam.sDcfmScan.uList.asPanDescr[j].u8SecurityFailure; //not added
                // scanParams[pos++] = psMlmeInd->uParam.sDcfmScan.uList.asPanDescr[j].u8AclEntry;
                // // not part of the standard scanParams[pos++] =
                // psMlmeInd->uParam.sDcfmScan.uList.asPanDescr[j].u8SecurityUse; // not part of the
                // standard
            }
        }
    }

    SendBytes(scanParams, paramsSize);
}

void
OrphanIndication(MAC_MlmeDcfmInd_s* psMlmeInd)
{
    uint8_t orphanIndParams[11];
    uint16_t pos = 0;
    uint32_t orphanAddressH = psMlmeInd->uParam.sIndOrphan.sDeviceAddr.u32H;
    uint32_t orphanAddressL = psMlmeInd->uParam.sIndOrphan.sDeviceAddr.u32L;

    orphanIndParams[pos++] = 0xAA;
    orphanIndParams[pos++] = ORPHAN_IND;
    // Parameter Size = OrphanAddress (8)
    orphanIndParams[pos++] = 8; // total size of the parameters in bytes.

    orphanIndParams[pos++] = (orphanAddressL & 0xFF);
    orphanIndParams[pos++] = (orphanAddressL >> 8) & 0xFF;
    orphanIndParams[pos++] = (orphanAddressL >> 16) & 0xFF;
    orphanIndParams[pos++] = (orphanAddressL >> 24) & 0xFF;

    orphanIndParams[pos++] = (orphanAddressH & 0xFF);
    orphanIndParams[pos++] = (orphanAddressH >> 8) & 0xFF;
    orphanIndParams[pos++] = (orphanAddressH >> 16) & 0xFF;
    orphanIndParams[pos++] = (orphanAddressH >> 24) & 0xFF;

    SendBytes(orphanIndParams, 11);
}

void
BeaconNotifyIndication(MAC_MlmeDcfmInd_s* psMlmeInd)
{
    uint16_t pos = 0;
    uint8_t paramSize = 0;

    // Check the total size to transmit through UART

    // Initial 0xAA byte (1) + Primitive Type (1) + ParamSize(1) + BSN (1) +
    // pending address specification (1) + SDU length (1)
    paramSize = 6;

    // PAN Descriptor Size
    // Coord address mode (1) + PAN ID (2) + LogChannel (1) + LogPage (1)
    // SuperframeSpec (2) + Gts permit (1) + Link quality (1) + TimeStamp (4) = 13
    paramSize += 13;

    uint8_t coordAddrMode = psMlmeInd->uParam.sIndBeacon.sPANdescriptor.sCoord.u8AddrMode;
    if (coordAddrMode == EXT_ADDR)
    {
        paramSize += 8;
    }
    else
    {
        paramSize += 2;
    }

    // Pending Addresses size (See IEEE 802.15.4-2011, Table 45)
    uint8_t pendingAddrSpecField = psMlmeInd->uParam.sIndBeacon.u8PendAddrSpec;
    uint8_t numPendingShortAddr = pendingAddrSpecField & (0x07);    // bits 0-2
    uint8_t numPendingExtAddr = (pendingAddrSpecField & 0x70) >> 4; // bits 4-6;

    paramSize += numPendingShortAddr * 2;
    paramSize += numPendingExtAddr * 8;

    // Beacon Payload Size
    paramSize += psMlmeInd->uParam.sIndBeacon.u8SDUlength;

    // From this point add the bytes to the array
    // representing the beacon received

    uint8_t beaconNotifyParams[paramSize];
    beaconNotifyParams[pos++] = 0xAA;
    beaconNotifyParams[pos++] = BEACON_NOTIFY_IND;
    // The size in bytes of the parameters (excluding 0xAA byte, the primitive type, and this value)
    beaconNotifyParams[pos++] = paramSize - 3;
    // BSN
    beaconNotifyParams[pos++] = psMlmeInd->uParam.sIndBeacon.u8BSN;
    // PAN Descriptor: Coordinator Address mode
    beaconNotifyParams[pos++] = coordAddrMode;
    // PAN Descriptor: Coordinator PAN ID
    beaconNotifyParams[pos++] =
        (psMlmeInd->uParam.sIndBeacon.sPANdescriptor.sCoord.u16PanId) & 0xFF;
    beaconNotifyParams[pos++] =
        (psMlmeInd->uParam.sIndBeacon.sPANdescriptor.sCoord.u16PanId >> 8) & 0xFF;
    // PAN Descriptor: Coordinator Address
    if (coordAddrMode == SHORT_ADDR)
    {
        // Coordinator short address
        beaconNotifyParams[pos++] =
            (psMlmeInd->uParam.sIndBeacon.sPANdescriptor.sCoord.uAddr.u16Short) && 0xFF;
        beaconNotifyParams[pos++] =
            (psMlmeInd->uParam.sIndBeacon.sPANdescriptor.sCoord.uAddr.u16Short >> 8) && 0xFF;
    }
    else
    {
        // Coordinator ext address
        beaconNotifyParams[pos++] =
            (psMlmeInd->uParam.sIndBeacon.sPANdescriptor.sCoord.uAddr.sExt.u32H) && 0xFF;
        beaconNotifyParams[pos++] =
            (psMlmeInd->uParam.sIndBeacon.sPANdescriptor.sCoord.uAddr.sExt.u32H >> 8) && 0xFF;
        beaconNotifyParams[pos++] =
            (psMlmeInd->uParam.sIndBeacon.sPANdescriptor.sCoord.uAddr.sExt.u32H >> 16) && 0xFF;
        beaconNotifyParams[pos++] =
            (psMlmeInd->uParam.sIndBeacon.sPANdescriptor.sCoord.uAddr.sExt.u32H >> 24) && 0xFF;

        beaconNotifyParams[pos++] =
            (psMlmeInd->uParam.sIndBeacon.sPANdescriptor.sCoord.uAddr.sExt.u32L) && 0xFF;
        beaconNotifyParams[pos++] =
            (psMlmeInd->uParam.sIndBeacon.sPANdescriptor.sCoord.uAddr.sExt.u32L >> 8) && 0xFF;
        beaconNotifyParams[pos++] =
            (psMlmeInd->uParam.sIndBeacon.sPANdescriptor.sCoord.uAddr.sExt.u32L >> 16) && 0xFF;
        beaconNotifyParams[pos++] =
            (psMlmeInd->uParam.sIndBeacon.sPANdescriptor.sCoord.uAddr.sExt.u32L >> 24) && 0xFF;
    }
    // PAN Descriptor: Channel Number
    beaconNotifyParams[pos++] = psMlmeInd->uParam.sIndBeacon.sPANdescriptor.u8LogicalChan;
    // PAN Descriptor: Channel Page
    beaconNotifyParams[pos++] = 0;
    // PAN Descriptor: Superframe Specification
    beaconNotifyParams[pos++] =
        (psMlmeInd->uParam.sIndBeacon.sPANdescriptor.u16SuperframeSpec) & 0xFF;
    beaconNotifyParams[pos++] =
        (psMlmeInd->uParam.sIndBeacon.sPANdescriptor.u16SuperframeSpec >> 8) & 0xFF;
    // PAN Descriptor: GTS permit
    beaconNotifyParams[pos++] = psMlmeInd->uParam.sIndBeacon.sPANdescriptor.u8GtsPermit;
    // PAN Descriptor: LQI
    beaconNotifyParams[pos++] = psMlmeInd->uParam.sIndBeacon.sPANdescriptor.u8LinkQuality;
    // PAN Descriptor: Time stamp
    beaconNotifyParams[pos++] = (psMlmeInd->uParam.sIndBeacon.sPANdescriptor.u32TimeStamp) & 0xFF;
    beaconNotifyParams[pos++] =
        (psMlmeInd->uParam.sIndBeacon.sPANdescriptor.u32TimeStamp >> 8) & 0xFF;
    beaconNotifyParams[pos++] =
        (psMlmeInd->uParam.sIndBeacon.sPANdescriptor.u32TimeStamp >> 16) & 0xFF;
    beaconNotifyParams[pos++] =
        (psMlmeInd->uParam.sIndBeacon.sPANdescriptor.u32TimeStamp >> 24) & 0xFF;
    // PAN Descriptor: Security
    // psMlmeInd->uParam.sIndBeacon.sPANdescriptor.u8SecurityFailure; // not supported
    // psMlmeInd->uParam.sIndBeacon.sPANdescriptor.u8SecurityUse; // not supported
    // Pending Address Specification Field
    beaconNotifyParams[pos++] = pendingAddrSpecField;
    uint8_t i = 0;
    for (i = 0; i < numPendingShortAddr; i++)
    {
        // Pending Short addresses
        beaconNotifyParams[pos++] = (psMlmeInd->uParam.sIndBeacon.uAddrList[i].u16Short) && 0xFF;
        beaconNotifyParams[pos++] =
            (psMlmeInd->uParam.sIndBeacon.uAddrList[i].u16Short >> 8) && 0xFF;
    }

    uint8_t j = 0;
    for (j = 0; j < numPendingExtAddr; j++)
    {
        // Pending Ext Addresses
        beaconNotifyParams[pos++] = (psMlmeInd->uParam.sIndBeacon.uAddrList[i].sExt.u32H) && 0xFF;
        beaconNotifyParams[pos++] =
            (psMlmeInd->uParam.sIndBeacon.uAddrList[i].sExt.u32H >> 8) && 0xFF;
        beaconNotifyParams[pos++] =
            (psMlmeInd->uParam.sIndBeacon.uAddrList[i].sExt.u32H >> 16) && 0xFF;
        beaconNotifyParams[pos++] =
            (psMlmeInd->uParam.sIndBeacon.uAddrList[i].sExt.u32H >> 24) && 0xFF;

        beaconNotifyParams[pos++] = (psMlmeInd->uParam.sIndBeacon.uAddrList[i].sExt.u32L) && 0xFF;
        beaconNotifyParams[pos++] =
            (psMlmeInd->uParam.sIndBeacon.uAddrList[i].sExt.u32L >> 8) && 0xFF;
        beaconNotifyParams[pos++] =
            (psMlmeInd->uParam.sIndBeacon.uAddrList[i].sExt.u32L >> 16) && 0xFF;
        beaconNotifyParams[pos++] =
            (psMlmeInd->uParam.sIndBeacon.uAddrList[i].sExt.u32L >> 24) && 0xFF;
    }

    // SDU Length (Beacon payload length)
    beaconNotifyParams[pos++] = psMlmeInd->uParam.sIndBeacon.u8SDUlength;
    uint8_t z = 0;
    for (z = 0; z < psMlmeInd->uParam.sIndBeacon.u8SDUlength; z++)
    {
        beaconNotifyParams[pos++] = psMlmeInd->uParam.sIndBeacon.u8SDU[z];
    }

    SendBytes(beaconNotifyParams, paramSize);
}

void
ProcessIncomingMlme(MAC_MlmeDcfmInd_s* psMlmeInd)
{
    // Handle MLME responses which are not synchronized (deferred)
    // We check the type of deferred mlme confirm or indication and process accordingly
    switch (psMlmeInd->u8Type)
    {
    case MAC_MLME_DCFM_SCAN:
        ScanConfirm(psMlmeInd);
        break;
    case MAC_MLME_DCFM_ASSOCIATE:
        AssociateConfirm(psMlmeInd);
        break;
    case MAC_MLME_IND_ASSOCIATE:
        AssociateIndication(psMlmeInd);
        break;
    case MAC_MLME_IND_SYNC_LOSS:
        SyncLossIndication(psMlmeInd);
        break;
    case MAC_MLME_IND_COMM_STATUS:
        CommStatusIndication(psMlmeInd);
        break;
    case MAC_MLME_IND_ORPHAN:
        OrphanIndication(psMlmeInd);
        break;
    case MAC_MLME_IND_BEACON_NOTIFY:
        BeaconNotifyIndication(psMlmeInd);
        break;
    default:
        break;
    }
}

void
ProcessIncomingMcps(MAC_McpsDcfmInd_s* psMcpsInd)
{
    switch (psMcpsInd->u8Type)
    {
    case MAC_MCPS_IND_DATA: {
        MAC_RxFrameData_s* psFrame;
        psFrame = &psMcpsInd->uParam.sIndData.sFrame;

        uint8_t pos = 0;
        uint8_t srcAddrMode = psFrame->sSrcAddr.u8AddrMode;
        uint16_t srcPanId = psFrame->sSrcAddr.u16PanId;
        uint8_t dstAddrMode = psFrame->sDstAddr.u8AddrMode;
        uint16_t dstPanId = psFrame->sDstAddr.u16PanId;

        // Parameter Size = 9 bytes + variable
        // SrcAddrMode (1) + SrcPanId (2) + SrdAddr (variable) +
        // DstAddrMode (1) + DstPanId (2) + DstAddr (variable) +
        // LQI (1) + DSN (1) + Msdu Length (1)+ Msdu (variable)
        uint8_t parametersSize = 9;

        if (srcAddrMode == EXT_ADDR)
        {
            parametersSize += 8;
        }
        else
        {
            parametersSize += 2;
        }

        if (dstAddrMode == EXT_ADDR)
        {
            parametersSize += 8;
        }
        else
        {
            parametersSize += 2;
        }

        parametersSize += psFrame->u8SduLength;

        uint8_t dataIndParams[parametersSize + 3];

        dataIndParams[pos++] = 0xAA;
        dataIndParams[pos++] = DATA_IND;
        dataIndParams[pos++] = parametersSize;
        dataIndParams[pos++] = srcAddrMode;
        dataIndParams[pos++] = (srcPanId & 0xFF);
        dataIndParams[pos++] = (srcPanId >> 8) & 0xFF;
        if (srcAddrMode == EXT_ADDR)
        {
            uint32_t srcAddr32H = psFrame->sSrcAddr.uAddr.sExt.u32H;
            uint32_t srcAddr32L = psFrame->sSrcAddr.uAddr.sExt.u32L;
            dataIndParams[pos++] = (srcAddr32L & 0xFF);
            dataIndParams[pos++] = (srcAddr32L >> 8) & 0xFF;
            dataIndParams[pos++] = (srcAddr32L >> 16) & 0xFF;
            dataIndParams[pos++] = (srcAddr32L >> 24) & 0xFF;

            dataIndParams[pos++] = (srcAddr32H & 0xFF);
            dataIndParams[pos++] = (srcAddr32H >> 8) & 0xFF;
            dataIndParams[pos++] = (srcAddr32H >> 16) & 0xFF;
            dataIndParams[pos++] = (srcAddr32H >> 24) & 0xFF;
        }
        else
        {
            uint16_t srcAddr = psFrame->sSrcAddr.uAddr.u16Short;
            dataIndParams[pos++] = (srcAddr & 0xFF);
            dataIndParams[pos++] = (srcAddr >> 8) & 0xFF;
        }
        dataIndParams[pos++] = dstAddrMode;
        dataIndParams[pos++] = (dstPanId & 0xFF);
        dataIndParams[pos++] = (dstPanId >> 8) & 0xFF;
        if (dstAddrMode == EXT_ADDR)
        {
            uint32_t dstAddr32H = psFrame->sDstAddr.uAddr.sExt.u32H;
            uint32_t dstAddr32L = psFrame->sDstAddr.uAddr.sExt.u32L;
            dataIndParams[pos++] = (dstAddr32L & 0xFF);
            dataIndParams[pos++] = (dstAddr32L >> 8) & 0xFF;
            dataIndParams[pos++] = (dstAddr32L >> 16) & 0xFF;
            dataIndParams[pos++] = (dstAddr32L >> 24) & 0xFF;

            dataIndParams[pos++] = (dstAddr32H & 0xFF);
            dataIndParams[pos++] = (dstAddr32H >> 8) & 0xFF;
            dataIndParams[pos++] = (dstAddr32H >> 16) & 0xFF;
            dataIndParams[pos++] = (dstAddr32H >> 24) & 0xFF;
        }
        else
        {
            uint16_t dstAddr = psFrame->sDstAddr.uAddr.u16Short;
            dataIndParams[pos++] = (dstAddr & 0xFF);
            dataIndParams[pos++] = (dstAddr >> 8) & 0xFF;
        }
        dataIndParams[pos++] = psFrame->u8LinkQuality;
        dataIndParams[pos++] = psFrame->u8DSN;
        dataIndParams[pos++] = psFrame->u8SduLength;
        // DBG_vPrintf(TRUE,"InsideDataInd: sduLenght %d %c%c",
        // psFrame->u8SduLength,psFrame->au8Sdu[0],psFrame->au8Sdu[1]);
        uint8_t i;
        for (i = 0; i < psFrame->u8SduLength; i++)
        {
            dataIndParams[pos++] = psFrame->au8Sdu[i];
        }

        SendBytes(dataIndParams, parametersSize + 3);
        break;
    }
    case MAC_MCPS_DCFM_DATA: {
        uint8_t pos = 0;
        uint8_t dataCfmParams[5];
        // DBG_vPrintf(TRUE,"InsideAssocCFM: %x %d",
        // psMlmeInd->uParam.sDcfmAssociate.u16AssocShortAddr,psMlmeInd->uParam.sDcfmAssociate.u8Status);
        dataCfmParams[pos++] = 0xAA;
        dataCfmParams[pos++] = DATA_CFM;
        // Parameter Size = 2
        // Status (1) + Handle (2)
        dataCfmParams[pos++] = 2;
        dataCfmParams[pos++] = psMcpsInd->uParam.sDcfmData.u8Status;
        dataCfmParams[pos++] = psMcpsInd->uParam.sDcfmData.u8Handle;
        // TODO Add timestamp when supported.
        SendBytes(dataCfmParams, 5);
        break;
    }
    default:
        break;
    }
}

void
AssociateRequest()
{
    uint16_t pos = 0;
    m_mlmeReqRsp.u8ParamLength = sizeof(MAC_MlmeReqAssociate_s);

    m_mlmeReqRsp.uParam.sReqAssociate.u8LogicalChan = m_rxBuffer[pos++];
    pos++; // In this device only Page 0 is supported, therefore, we ignore this byte.
    m_mlmeReqRsp.uParam.sReqAssociate.sCoord.u8AddrMode = m_rxBuffer[pos++];

    m_mlmeReqRsp.uParam.sReqAssociate.sCoord.u16PanId = m_rxBuffer[pos++];
    m_mlmeReqRsp.uParam.sReqAssociate.sCoord.u16PanId |= ((m_rxBuffer[pos++]) << 8);

    m_mlmeReqRsp.uParam.sReqAssociate.u8Capability = m_rxBuffer[pos++];

    if (m_mlmeReqRsp.uParam.sReqAssociate.sCoord.u8AddrMode == EXT_ADDR)
    {
        // Extended address
        m_mlmeReqRsp.uParam.sReqAssociate.sCoord.uAddr.sExt.u32H = m_rxBuffer[pos++];
        m_mlmeReqRsp.uParam.sReqAssociate.sCoord.uAddr.sExt.u32H |= ((m_rxBuffer[pos++]) << 8);
        m_mlmeReqRsp.uParam.sReqAssociate.sCoord.uAddr.sExt.u32H |= ((m_rxBuffer[pos++]) << 16);
        m_mlmeReqRsp.uParam.sReqAssociate.sCoord.uAddr.sExt.u32H |= ((m_rxBuffer[pos++]) << 24);

        m_mlmeReqRsp.uParam.sReqAssociate.sCoord.uAddr.sExt.u32L = m_rxBuffer[pos++];
        m_mlmeReqRsp.uParam.sReqAssociate.sCoord.uAddr.sExt.u32L |= ((m_rxBuffer[pos++]) << 8);
        m_mlmeReqRsp.uParam.sReqAssociate.sCoord.uAddr.sExt.u32L |= ((m_rxBuffer[pos++]) << 16);
        m_mlmeReqRsp.uParam.sReqAssociate.sCoord.uAddr.sExt.u32L |= ((m_rxBuffer[pos++]) << 24);
    }
    else if (m_mlmeReqRsp.uParam.sReqAssociate.sCoord.u8AddrMode == SHORT_ADDR)
    {
        // Short address
        m_mlmeReqRsp.uParam.sReqAssociate.sCoord.uAddr.u16Short = m_rxBuffer[pos++];
        m_mlmeReqRsp.uParam.sReqAssociate.sCoord.uAddr.u16Short |= ((m_rxBuffer[pos++]) << 8);
    }

    m_mlmeReqRsp.uParam.sReqAssociate.u8SecurityEnable = FALSE;
    // TODO: Add security information here when supported.
    // DBG_vPrintf(TRUE,"Process Data: %d %x %x %x %x \n
    // ",m_mlmeReqRsp.uParam.sReqAssociate.u8LogicalChan,m_mlmeReqRsp.uParam.sReqAssociate.u8Capability,
    // m_mlmeReqRsp.uParam.sReqAssociate.sCoord.u8AddrMode,m_mlmeReqRsp.uParam.sReqAssociate.sCoord.u16PanId,m_mlmeReqRsp.uParam.sReqAssociate.sCoord.uAddr.u16Short);
    vAppApiMlmeRequest(&m_mlmeReqRsp, &m_mlmeSyncCfm);
}

void
GetRequest()
{
    uint8_t attributeId = m_rxBuffer[0];

    // Handle MLME responses and confirmation which are synchronized
    // Non-Synchronized responses (a.k.a Deferred) are handle in ProcessingIncomingMlme or
    // ProcessingIncomingMcps
    switch (attributeId)
    {
    case PHY_PIB_ATTR_CURRENT_CHANNEL: {
        uint32_t channel = 0;
        uint32_t* channelPtr = &channel;

        if (eAppApiPlmeGet(PHY_PIB_ATTR_CURRENT_CHANNEL, channelPtr) == PHY_ENUM_SUCCESS)
        {
            uint8_t getCfmParams[6];
            getCfmParams[0] = 0xAA;
            getCfmParams[1] = GET_CFM;
            getCfmParams[2] = 3; // parameters size
            getCfmParams[3] = MAC_ENUM_SUCCESS;
            getCfmParams[4] = attributeId;
            getCfmParams[5] = (uint8_t)channel;
            SendBytes(getCfmParams, 6);
        }
        break;
    }
    case 0x04: // Current Page,
    {
        // Only Page 0 is supported in JN5169
        uint8_t getCfmParams[6];
        getCfmParams[0] = 0xAA;
        getCfmParams[1] = GET_CFM;
        getCfmParams[2] = 3; // parameters size
        getCfmParams[3] = MAC_ENUM_SUCCESS;
        getCfmParams[4] = attributeId;
        getCfmParams[5] = 0; // Page value
        SendBytes(getCfmParams, 6);
        break;
    }
    case 0x6f: // Extended Address
    {
        // Received a request for extended address
        // This attribute is not compliant for 2003 and 2006 versions of the standard
        // but it was later on added to 2011 edition. In this shim implementation we make it
        // accessible through the MLME-GET.request function and the attribute id 0x6f as described
        // by 2015 and onwards editions of the standard.
        uint64_t extAddr = 0;
        uint8_t pos = 0;
        uint8_t getCfmParams[13];
        uint64_t* extAddrPtr = &extAddr;
        bAHI_ReadMACID(TRUE, extAddrPtr);

        getCfmParams[pos++] = 0xAA;
        getCfmParams[pos++] = GET_CFM;
        getCfmParams[pos++] = 10; // parameters size
        getCfmParams[pos++] = MAC_ENUM_SUCCESS;
        getCfmParams[pos++] = attributeId;

        getCfmParams[pos++] = (extAddr & 0xFF);
        getCfmParams[pos++] = (extAddr >> 8) & 0xFF;
        getCfmParams[pos++] = (extAddr >> 16) & 0xFF;
        getCfmParams[pos++] = (extAddr >> 24) & 0xFF;

        getCfmParams[pos++] = (extAddr >> 32) & 0xFF;
        getCfmParams[pos++] = (extAddr >> 40) & 0xFF;
        getCfmParams[pos++] = (extAddr >> 48) & 0xFF;
        getCfmParams[pos++] = (extAddr >> 56) & 0xFF;

        SendBytes(getCfmParams, 13);
        break;
    }
    case MAC_PIB_ATTR_ACK_WAIT_DURATION: {
        uint8_t getCfmParams[6];
        getCfmParams[0] = 0xAA;
        getCfmParams[1] = GET_CFM;
        getCfmParams[2] = 3; // parameters size
        getCfmParams[3] = MAC_ENUM_SUCCESS;
        getCfmParams[4] = attributeId;
        getCfmParams[5] = s_psMacPib->eAckWaitDuration;
        SendBytes(getCfmParams, 6);
        break;
    }
    case MAC_PIB_ATTR_ASSOCIATION_PERMIT: {
        uint8_t getCfmParams[6];
        getCfmParams[0] = 0xAA;
        getCfmParams[1] = GET_CFM;
        getCfmParams[2] = 3; // parameters size
        getCfmParams[3] = MAC_ENUM_SUCCESS;
        getCfmParams[4] = attributeId;
        getCfmParams[5] = s_psMacPib->bAssociationPermit;
        SendBytes(getCfmParams, 6);
        break;
    }
    case MAC_PIB_ATTR_AUTO_REQUEST: {
        uint8_t getCfmParams[6];
        getCfmParams[0] = 0xAA;
        getCfmParams[1] = GET_CFM;
        getCfmParams[2] = 3; // parameters size
        getCfmParams[3] = MAC_ENUM_SUCCESS;
        getCfmParams[4] = attributeId;
        getCfmParams[5] = s_psMacPib->bAutoRequest;
        SendBytes(getCfmParams, 6);
        break;
    }
    case MAC_PIB_ATTR_BATT_LIFE_EXT: {
        uint8_t getCfmParams[6];
        getCfmParams[0] = 0xAA;
        getCfmParams[1] = GET_CFM;
        getCfmParams[2] = 3; // parameters size
        getCfmParams[3] = MAC_ENUM_SUCCESS;
        getCfmParams[4] = attributeId;
        getCfmParams[5] = s_psMacPib->bBattLifeExt;
        SendBytes(getCfmParams, 6);
        break;
    }
    case MAC_PIB_ATTR_BATT_LIFE_EXT_PERIODS: {
        uint8_t getCfmParams[6];
        getCfmParams[0] = 0xAA;
        getCfmParams[1] = GET_CFM;
        getCfmParams[2] = 3; // parameters size
        getCfmParams[3] = MAC_ENUM_SUCCESS;
        getCfmParams[4] = attributeId;
        getCfmParams[5] = s_psMacPib->eBattLifeExtPeriods;
        SendBytes(getCfmParams, 6);
        break;
    }
    case MAC_PIB_ATTR_BEACON_PAYLOAD: {
        uint8_t beaconPayloadLength = s_psMacPib->u8BeaconPayloadLength;
        uint8_t getCfmParams[beaconPayloadLength + 5];
        uint8_t pos = 0;
        getCfmParams[pos++] = 0xAA;
        getCfmParams[pos++] = GET_CFM;
        getCfmParams[pos++] = beaconPayloadLength + 2; // parameters size
        getCfmParams[pos++] = MAC_ENUM_SUCCESS;
        getCfmParams[pos++] = attributeId;
        uint8_t i;
        for (i = 0; i < beaconPayloadLength; i++)
        {
            getCfmParams[pos++] = s_psMacPib->au8BeaconPayload[i];
        }
        SendBytes(getCfmParams, beaconPayloadLength + 5);
        break;
    }
    case MAC_PIB_ATTR_BEACON_PAYLOAD_LENGTH: {
        DBG_vPrintf(TRUE, "GET request beacon payload:  %d \n", s_psMacPib->u8BeaconPayloadLength);
        uint8_t getCfmParams[6];
        getCfmParams[0] = 0xAA;
        getCfmParams[1] = GET_CFM;
        getCfmParams[2] = 3; // parameters size
        getCfmParams[3] = MAC_ENUM_SUCCESS;
        getCfmParams[4] = attributeId;
        getCfmParams[5] = s_psMacPib->u8BeaconPayloadLength;
        SendBytes(getCfmParams, 6);
        break;
    }
    case MAC_PIB_ATTR_BEACON_ORDER: {
        uint8_t getCfmParams[6];
        getCfmParams[0] = 0xAA;
        getCfmParams[1] = GET_CFM;
        getCfmParams[2] = 3; // parameters size
        getCfmParams[3] = MAC_ENUM_SUCCESS;
        getCfmParams[4] = attributeId;
        getCfmParams[5] = s_psMacPib->u8BeaconOrder;
        SendBytes(getCfmParams, 6);
        break;
    }
    case MAC_PIB_ATTR_BEACON_TX_TIME: {
        uint32_t txTime = s_psMacPib->u32BeaconTxTime;
        uint8_t getCfmParams[9];
        getCfmParams[0] = 0xAA;
        getCfmParams[1] = GET_CFM;
        getCfmParams[2] = 6; // parameters size
        getCfmParams[3] = MAC_ENUM_SUCCESS;
        getCfmParams[4] = attributeId;

        getCfmParams[5] = (txTime & 0xFF);
        getCfmParams[6] = (txTime >> 8) & 0xFF;
        getCfmParams[7] = (txTime >> 16) & 0xFF;
        getCfmParams[8] = (txTime >> 24) & 0xFF;
        SendBytes(getCfmParams, 9);
        break;
    }
    case MAC_PIB_ATTR_BSN: {
        uint8_t getCfmParams[6];
        getCfmParams[0] = 0xAA;
        getCfmParams[1] = GET_CFM;
        getCfmParams[2] = 3; // parameters size
        getCfmParams[3] = MAC_ENUM_SUCCESS;
        getCfmParams[4] = attributeId;
        getCfmParams[5] = s_psMacPib->u8Bsn;
        SendBytes(getCfmParams, 6);
        break;
    }
    case MAC_PIB_ATTR_COORD_EXTENDED_ADDRESS: {
        uint32_t coordExtAddrH = s_psMacPib->sCoordExtAddr.u32H;
        uint32_t coordExtAddrL = s_psMacPib->sCoordExtAddr.u32L;

        uint8_t getCfmParams[13];
        getCfmParams[0] = 0xAA;
        getCfmParams[1] = GET_CFM;
        getCfmParams[2] = 10; // parameters size
        getCfmParams[3] = MAC_ENUM_SUCCESS;
        getCfmParams[4] = attributeId;

        getCfmParams[5] = (coordExtAddrL & 0xFF);
        getCfmParams[6] = (coordExtAddrL >> 8) & 0xFF;
        getCfmParams[7] = (coordExtAddrL >> 16) & 0xFF;
        getCfmParams[8] = (coordExtAddrL >> 24) & 0xFF;

        getCfmParams[9] = (coordExtAddrH & 0xFF);
        getCfmParams[10] = (coordExtAddrH >> 8) & 0xFF;
        getCfmParams[11] = (coordExtAddrH >> 16) & 0xFF;
        getCfmParams[12] = (coordExtAddrH >> 24) & 0xFF;
        SendBytes(getCfmParams, 13);
        break;
    }
    case MAC_PIB_ATTR_COORD_SHORT_ADDRESS: {
        uint8_t getCfmParams[7];
        getCfmParams[0] = 0xAA;
        getCfmParams[1] = GET_CFM;
        getCfmParams[2] = 4; // parameters size
        getCfmParams[3] = MAC_ENUM_SUCCESS;
        getCfmParams[4] = attributeId;
        getCfmParams[5] = (s_psMacPib->u16CoordShortAddr & 0xFF);
        getCfmParams[6] = (s_psMacPib->u16CoordShortAddr >> 8) & 0xFF;
        SendBytes(getCfmParams, 7);
        break;
    }
    case MAC_PIB_ATTR_DSN: {
        uint8_t getCfmParams[6];
        getCfmParams[0] = 0xAA;
        getCfmParams[1] = GET_CFM;
        getCfmParams[2] = 3; // parameters size
        getCfmParams[3] = MAC_ENUM_SUCCESS;
        getCfmParams[4] = attributeId;
        getCfmParams[5] = s_psMacPib->u8Dsn;
        SendBytes(getCfmParams, 6);
        break;
    }
    case MAC_PIB_ATTR_MAX_CSMA_BACKOFFS: {
        uint8_t getCfmParams[6];
        getCfmParams[0] = 0xAA;
        getCfmParams[1] = GET_CFM;
        getCfmParams[2] = 3; // parameters size
        getCfmParams[3] = MAC_ENUM_SUCCESS;
        getCfmParams[4] = attributeId;
        getCfmParams[5] = s_psMacPib->u8MaxCsmaBackoffs_ReadOnly;
        SendBytes(getCfmParams, 6);
        break;
    }
    case MAC_PIB_ATTR_MIN_BE: {
        uint8_t getCfmParams[6];
        getCfmParams[0] = 0xAA;
        getCfmParams[1] = GET_CFM;
        getCfmParams[2] = 3; // parameters size
        getCfmParams[3] = MAC_ENUM_SUCCESS;
        getCfmParams[4] = attributeId;
        getCfmParams[5] = s_psMacPib->u8MinBe_ReadOnly;
        SendBytes(getCfmParams, 6);
        break;
    }
    case MAC_PIB_ATTR_PAN_ID: {
        uint8_t getCfmParams[7];
        getCfmParams[0] = 0xAA;
        getCfmParams[1] = GET_CFM;
        getCfmParams[2] = 4; // size of parameters (4 bytes)
        getCfmParams[3] = MAC_ENUM_SUCCESS;
        getCfmParams[4] = attributeId;
        getCfmParams[5] = (s_psMacPib->u16PanId_ReadOnly & 0xFF);
        getCfmParams[6] = (s_psMacPib->u16PanId_ReadOnly >> 8) & 0xFF;
        SendBytes(getCfmParams, 7);
        break;
    }
    case MAC_PIB_ATTR_PROMISCUOUS_MODE: {
        uint8_t getCfmParams[6];
        getCfmParams[0] = 0xAA;
        getCfmParams[1] = GET_CFM;
        getCfmParams[2] = 3; // parameters size
        getCfmParams[3] = MAC_ENUM_SUCCESS;
        getCfmParams[4] = attributeId;
        getCfmParams[5] = s_psMacPib->bPromiscuousMode_ReadOnly;
        SendBytes(getCfmParams, 6);
        break;
    }
    case MAC_PIB_ATTR_RX_ON_WHEN_IDLE: {
        uint8_t getCfmParams[6];
        getCfmParams[0] = 0xAA;
        getCfmParams[1] = GET_CFM;
        getCfmParams[2] = 3; // parameters size
        getCfmParams[3] = MAC_ENUM_SUCCESS;
        getCfmParams[4] = attributeId;
        getCfmParams[5] = s_psMacPib->bRxOnWhenIdle_ReadOnly;
        SendBytes(getCfmParams, 6);
        break;
    }
    case MAC_PIB_ATTR_SHORT_ADDRESS: {
        uint16_t shortAddress = s_psMacPib->u16ShortAddr_ReadOnly;
        uint8_t getCfmParams[7];
        getCfmParams[0] = 0xAA;
        getCfmParams[1] = GET_CFM;
        getCfmParams[2] = 4; // parameters size
        getCfmParams[3] = MAC_ENUM_SUCCESS;
        getCfmParams[4] = attributeId;

        getCfmParams[5] = (shortAddress & 0xFF);
        getCfmParams[6] = (shortAddress >> 8) & 0xFF;
        SendBytes(getCfmParams, 7);
        break;
    }
    case MAC_PIB_ATTR_SUPERFRAME_ORDER: {
        uint8_t getCfmParams[6];
        getCfmParams[0] = 0xAA;
        getCfmParams[1] = GET_CFM;
        getCfmParams[2] = 3; // parameters size
        getCfmParams[3] = MAC_ENUM_SUCCESS;
        getCfmParams[4] = attributeId;
        getCfmParams[5] = s_psMacPib->u8SuperframeOrder;
        SendBytes(getCfmParams, 6);
        break;
    }
    case MAC_PIB_ATTR_TRANSACTION_PERSISTENCE_TIME: {
        uint8_t getCfmParams[6];
        getCfmParams[0] = 0xAA;
        getCfmParams[1] = GET_CFM;
        getCfmParams[2] = 3; // parameters size
        getCfmParams[3] = MAC_ENUM_SUCCESS;
        getCfmParams[4] = attributeId;
        getCfmParams[5] = s_psMacPib->u16TransactionPersistenceTime;
        SendBytes(getCfmParams, 6);
        break;
    }
    case MAC_PIB_ATTR_MAX_FRAME_RETRIES: {
        uint8_t getCfmParams[6];
        getCfmParams[0] = 0xAA;
        getCfmParams[1] = GET_CFM;
        getCfmParams[2] = 3; // parameters size
        getCfmParams[3] = MAC_ENUM_SUCCESS;
        getCfmParams[4] = attributeId;
        getCfmParams[5] = s_psMacPib->u8MaxFrameRetries;
        SendBytes(getCfmParams, 6);
        break;
    }
    case MAC_PIB_ATTR_RESPONSE_WAIT_TIME: {
        uint8_t getCfmParams[6];
        getCfmParams[0] = 0xAA;
        getCfmParams[1] = GET_CFM;
        getCfmParams[2] = 3; // parameters size
        getCfmParams[3] = MAC_ENUM_SUCCESS;
        getCfmParams[4] = attributeId;
        getCfmParams[5] = s_psMacPib->u8ResponseWaitTime;
        SendBytes(getCfmParams, 6);
        break;
    }
    case MAC_PIB_ATTR_MACFRAMECOUNTER: {
        uint32_t frameCounter = s_psMacPib->u32MacFrameCounter;
        uint8_t getCfmParams[9];
        getCfmParams[0] = 0xAA;
        getCfmParams[1] = GET_CFM;
        getCfmParams[2] = 6; // parameters size
        getCfmParams[3] = MAC_ENUM_SUCCESS;
        getCfmParams[4] = attributeId;

        getCfmParams[5] = (frameCounter & 0xFF);
        getCfmParams[6] = (frameCounter >> 8) & 0xFF;
        getCfmParams[7] = (frameCounter >> 16) & 0xFF;
        getCfmParams[8] = (frameCounter >> 24) & 0xFF;
        SendBytes(getCfmParams, 9);
        break;
    }
    default: {
        uint8_t getCfmParams[5];
        getCfmParams[0] = 0xAA;
        getCfmParams[1] = GET_CFM;
        getCfmParams[2] = 2; // parameters size
        getCfmParams[3] = MAC_ENUM_UNSUPPORTED_ATTRIBUTE;
        getCfmParams[4] = attributeId;
        SendBytes(getCfmParams, 5);
        break;
    }
    }
}

void
ScanRequest()
{
    m_mlmeReqRsp.u8ParamLength = sizeof(MAC_MlmeReqScan_s);
    m_mlmeReqRsp.uParam.sReqScan.u32ScanChannels =
        m_rxBuffer[0] + (m_rxBuffer[1] << 8) + (m_rxBuffer[2] << 16) + (m_rxBuffer[3] << 24);
    m_mlmeReqRsp.uParam.sReqScan.u8ScanType = m_rxBuffer[4];
    m_mlmeReqRsp.uParam.sReqScan.u8ScanDuration = m_rxBuffer[5];

    // TODO: Add security information here when supported.

    // DBG_vPrintf(TRUE,"Process Data: %x %d %d \n
    // ",m_mlmeReqRsp.uParam.sReqScan.u32ScanChannels,m_mlmeReqRsp.uParam.sReqScan.u8ScanDuration,m_mlmeReqRsp.uParam.sReqScan.u8ScanType);
    vAppApiMlmeRequest(&m_mlmeReqRsp, &m_mlmeSyncCfm);
}

void
SetRequest()
{
    uint8_t pos = 0;
    uint8_t attributeId = m_rxBuffer[pos++];
    uint8_t status = MAC_ENUM_SUCCESS;

    switch (attributeId)
    {
    case PHY_PIB_ATTR_CURRENT_CHANNEL: {
        uint32 channel = (uint32)m_rxBuffer[pos++];
        if (eAppApiPlmeSet(PHY_PIB_ATTR_CURRENT_CHANNEL, channel) != PHY_ENUM_SUCCESS)
        {
            status = MAC_ENUM_INVALID_PARAMETER;
        }
        break;
    }
    case 0x04: // Current Page (No enumeration assigned in JN5169)
    {
        if (m_rxBuffer[pos++] != 0)
        {
            status = MAC_ENUM_UNSUPPORTED_ATTRIBUTE;
        }
        break;
    }
    case MAC_PIB_ATTR_ACK_WAIT_DURATION:
        s_psMacPib->eAckWaitDuration = m_rxBuffer[pos++];
        break;
    case MAC_PIB_ATTR_ASSOCIATION_PERMIT:
        s_psMacPib->bAssociationPermit = m_rxBuffer[pos++];
        break;
    case MAC_PIB_ATTR_AUTO_REQUEST:
        s_psMacPib->bAutoRequest = m_rxBuffer[pos++];
        break;
    case MAC_PIB_ATTR_BATT_LIFE_EXT:
        s_psMacPib->bBattLifeExt = m_rxBuffer[pos++];
        break;
    case MAC_PIB_ATTR_BATT_LIFE_EXT_PERIODS:
        s_psMacPib->eBattLifeExtPeriods = m_rxBuffer[pos++];
        break;
    case MAC_PIB_ATTR_BEACON_PAYLOAD: {
        if (s_psMacPib->u8BeaconPayloadLength == 0 ||
            s_psMacPib->u8BeaconPayloadLength > MAC_MAX_BEACON_PAYLOAD_LEN)
        {
            // The maximum beacon payload size is 118 bytes.
            status = MAC_ENUM_UNSUPPORTED_ATTRIBUTE;
        }
        else
        {
            uint8_t i;
            uint8_t payloadLength = s_psMacPib->u8BeaconPayloadLength;
            for (i = 0; i < payloadLength; i++)
            {
                s_psMacPib->au8BeaconPayload[i] = m_rxBuffer[pos++];
            }
        }
        break;
    }
    case MAC_PIB_ATTR_BEACON_PAYLOAD_LENGTH:
        s_psMacPib->u8BeaconPayloadLength = m_rxBuffer[pos++];
        // DBG_vPrintf(TRUE,"Set request beacon payload:  %d \n",s_psMacPib->u8BeaconPayloadLength);
        break;
    case MAC_PIB_ATTR_BEACON_ORDER:
        s_psMacPib->u8BeaconOrder = m_rxBuffer[pos++];
        break;
    case MAC_PIB_ATTR_BEACON_TX_TIME: {
        uint32_t txTime;
        txTime = m_rxBuffer[pos++];
        txTime |= ((m_rxBuffer[pos++]) << 8);
        txTime |= ((m_rxBuffer[pos++]) << 16);
        txTime |= ((m_rxBuffer[pos++]) << 24);
        s_psMacPib->u32BeaconTxTime = txTime;
        break;
    }
    case MAC_PIB_ATTR_BSN:
        s_psMacPib->u8Bsn = m_rxBuffer[pos++];
        break;
    case MAC_PIB_ATTR_COORD_EXTENDED_ADDRESS: {
        uint32_t coordExtAddrH;
        uint32_t coordExtAddrL;

        coordExtAddrL = m_rxBuffer[pos++];
        coordExtAddrL |= ((m_rxBuffer[pos++]) << 8);
        coordExtAddrL |= ((m_rxBuffer[pos++]) << 16);
        coordExtAddrL |= ((m_rxBuffer[pos++]) << 24);

        coordExtAddrH = m_rxBuffer[pos++];
        coordExtAddrH |= ((m_rxBuffer[pos++]) << 8);
        coordExtAddrH |= ((m_rxBuffer[pos++]) << 16);
        coordExtAddrH |= ((m_rxBuffer[pos++]) << 24);

        s_psMacPib->sCoordExtAddr.u32H = coordExtAddrH;
        s_psMacPib->sCoordExtAddr.u32L = coordExtAddrL;
        break;
    }
    case MAC_PIB_ATTR_COORD_SHORT_ADDRESS: {
        uint16_t coordShortAddr;
        coordShortAddr = m_rxBuffer[pos++];
        coordShortAddr |= ((m_rxBuffer[pos++]) << 8);
        s_psMacPib->u16CoordShortAddr = coordShortAddr;
        break;
    }
    case MAC_PIB_ATTR_DSN:
        s_psMacPib->u8Dsn = m_rxBuffer[pos++];
        break;
    case MAC_PIB_ATTR_GTS_PERMIT:
        // not found
        // todo
        break;
    case MAC_PIB_ATTR_MAX_CSMA_BACKOFFS:
        MAC_vPibSetMaxCsmaBackoffs(s_pvMac, m_rxBuffer[pos++]);
        break;
    case MAC_PIB_ATTR_MIN_BE:
        MAC_vPibSetMinBe(s_pvMac, m_rxBuffer[pos++]);
        break;
    case MAC_PIB_ATTR_PAN_ID: {
        uint16_t panId;
        panId = m_rxBuffer[pos++];
        panId |= ((m_rxBuffer[pos++]) << 8);
        MAC_vPibSetPanId(s_pvMac, panId);
        break;
    }
    case MAC_PIB_ATTR_PROMISCUOUS_MODE:
        MAC_vPibSetPromiscuousMode(s_pvMac, m_rxBuffer[pos++], FALSE);
        break;
    case MAC_PIB_ATTR_RX_ON_WHEN_IDLE:
        MAC_vPibSetRxOnWhenIdle(s_pvMac, m_rxBuffer[pos++], FALSE);
        break;
    case MAC_PIB_ATTR_SHORT_ADDRESS: {
        uint16_t shortAddress;
        shortAddress = m_rxBuffer[pos++];
        shortAddress |= ((m_rxBuffer[pos++]) << 8);
        MAC_vPibSetShortAddr(s_pvMac, shortAddress);
        break;
    }
    case MAC_PIB_ATTR_SUPERFRAME_ORDER:
        s_psMacPib->u8SuperframeOrder = m_rxBuffer[pos++];
        break;
    case MAC_PIB_ATTR_TRANSACTION_PERSISTENCE_TIME: {
        uint16_t persistenceTime;
        persistenceTime = m_rxBuffer[pos++];
        persistenceTime |= ((m_rxBuffer[pos++]) << 8);
        s_psMacPib->u16TransactionPersistenceTime = persistenceTime;
        break;
    }
    case MAC_PIB_ATTR_MAX_FRAME_RETRIES:
        s_psMacPib->u8MaxFrameRetries = m_rxBuffer[pos++];
        break;
    case MAC_PIB_ATTR_RESPONSE_WAIT_TIME:
        s_psMacPib->u8ResponseWaitTime = m_rxBuffer[pos++];
        break;
    case MAC_PIB_ATTR_MACFRAMECOUNTER: {
        uint32_t frameCounter;
        frameCounter = m_rxBuffer[pos++];
        frameCounter |= ((m_rxBuffer[pos++]) << 8);
        frameCounter |= ((m_rxBuffer[pos++]) << 16);
        frameCounter |= ((m_rxBuffer[pos++]) << 24);
        s_psMacPib->u32MacFrameCounter = frameCounter;
        break;
    }
    default:
        status = MAC_ENUM_UNSUPPORTED_ATTRIBUTE;
        break;
    }

    // Handle MLME responses and confirmation which are synchronized
    // Non-Synchronized responses (a.k.a Deferred) are handle in ProcessingIncomingMlme or
    // ProcessingIncomingMcps
    uint8_t setCfmParams[5];
    setCfmParams[0] = 0xAA;
    setCfmParams[1] = SET_CFM;
    setCfmParams[2] = 2; // size of parameters (2 bytes)
    setCfmParams[3] = attributeId;
    setCfmParams[4] = status;
    SendBytes(setCfmParams, 5);
}

void
StartRequest()
{
    // TODO: delete this and set externally.
    /* Enable receiver to be on when idle */
    MAC_vPibSetRxOnWhenIdle(s_pvMac, TRUE, FALSE);

    m_mlmeReqRsp.u8ParamLength = sizeof(MAC_MlmeReqStart_s);
    m_mlmeReqRsp.uParam.sReqStart.u16PanId = m_rxBuffer[0] + (m_rxBuffer[1] << 8);
    m_mlmeReqRsp.uParam.sReqStart.u8Channel = m_rxBuffer[2];
    // JN5169 only supports page 0, no need to set the page.
    m_mlmeReqRsp.uParam.sReqStart.u32StarTime =
        m_rxBuffer[4] + (m_rxBuffer[5] << 8) + (m_rxBuffer[6] << 16) + (m_rxBuffer[7] << 24);

    m_mlmeReqRsp.uParam.sReqStart.u8BeaconOrder = m_rxBuffer[8];
    m_mlmeReqRsp.uParam.sReqStart.u8SuperframeOrder = m_rxBuffer[9];
    m_mlmeReqRsp.uParam.sReqStart.u8PanCoordinator = m_rxBuffer[10];
    m_mlmeReqRsp.uParam.sReqStart.u8BatteryLifeExt = m_rxBuffer[11];
    m_mlmeReqRsp.uParam.sReqStart.u8Realignment = m_rxBuffer[12];
    m_mlmeReqRsp.uParam.sReqStart.u8SecurityEnable = FALSE;
    // DBG_vPrintf(TRUE,"In process data START request: %x %d %d %d %d %d %d
    // %d\n",m_mlmeReqRsp.uParam.sReqStart.u16PanId,m_mlmeReqRsp.uParam.sReqStart.u8Channel,m_mlmeReqRsp.uParam.sReqStart.u8BeaconOrder,m_mlmeReqRsp.uParam.sReqStart.u8SuperframeOrder,m_mlmeReqRsp.uParam.sReqStart.u8PanCoordinator,m_mlmeReqRsp.uParam.sReqStart.u8BatteryLifeExt,m_mlmeReqRsp.uParam.sReqStart.u8Realignment,m_mlmeReqRsp.uParam.sReqStart.u8SecurityEnable);
    vAppApiMlmeRequest(&m_mlmeReqRsp, &m_mlmeSyncCfm);

    // Handle MLME responses which are synchronized
    // Non-Synchronized responses (a.k.a Deferred) are handle in ProcessingIncomingMlme or
    // ProcessingIncomingMcps
    uint8_t startParams[4];
    startParams[0] = 0xAA;
    startParams[1] = START_CFM;
    startParams[2] = 1; // start Confirm returns only status (1 byte)
    startParams[3] = m_mlmeSyncCfm.uParam.sCfmStart.u8Status;
    SendBytes(startParams, 4);
    // DBG_vPrintf(TRUE,"mlme start confirm %x \n", m_mlmeSyncCfm.uParam.sCfmStart.u8Status);
}

void
AssociateResponse()
{
    uint16_t pos = 0;
    m_mlmeReqRsp.u8ParamLength = sizeof(MAC_MlmeRspAssociate_s);
    m_mlmeReqRsp.uParam.sRspAssociate.sDeviceAddr.u32L = m_rxBuffer[pos++];
    m_mlmeReqRsp.uParam.sRspAssociate.sDeviceAddr.u32L |= ((m_rxBuffer[pos++]) << 8);
    m_mlmeReqRsp.uParam.sRspAssociate.sDeviceAddr.u32L |= ((m_rxBuffer[pos++]) << 16);
    m_mlmeReqRsp.uParam.sRspAssociate.sDeviceAddr.u32L |= ((m_rxBuffer[pos++]) << 24);

    m_mlmeReqRsp.uParam.sRspAssociate.sDeviceAddr.u32H = m_rxBuffer[pos++];
    m_mlmeReqRsp.uParam.sRspAssociate.sDeviceAddr.u32H |= ((m_rxBuffer[pos++]) << 8);
    m_mlmeReqRsp.uParam.sRspAssociate.sDeviceAddr.u32H |= ((m_rxBuffer[pos++]) << 16);
    m_mlmeReqRsp.uParam.sRspAssociate.sDeviceAddr.u32H |= ((m_rxBuffer[pos++]) << 24);

    m_mlmeReqRsp.uParam.sRspAssociate.u16AssocShortAddr = m_rxBuffer[pos++];
    m_mlmeReqRsp.uParam.sRspAssociate.u16AssocShortAddr |= ((m_rxBuffer[pos++]) << 8);

    m_mlmeReqRsp.uParam.sRspAssociate.u8Status = m_rxBuffer[pos++];

    // DBG_vPrintf(TRUE,"InsideAssocResp: %x %x %d",
    // m_mlmeReqRsp.uParam.sRspAssociate.sDeviceAddr.u32L,
    // m_mlmeReqRsp.uParam.sRspAssociate.sDeviceAddr.u32H,m_mlmeReqRsp.uParam.sRspAssociate.u8Status);

    // Handle security parameters here when supported
    m_mlmeReqRsp.uParam.sRspAssociate.u8SecurityEnable = FALSE;
    vAppApiMlmeRequest(&m_mlmeReqRsp, &m_mlmeSyncCfm);
}

void
DataRequest()
{
    uint16_t pos = 0;

    m_mcpsReqRsp.u8ParamLength = sizeof(MAC_McpsReqData_s);

    m_mcpsReqRsp.uParam.sReqData.u8Handle = m_rxBuffer[pos++];
    m_mcpsReqRsp.uParam.sReqData.sFrame.sSrcAddr.u8AddrMode = m_rxBuffer[pos++];

    // According to the standard, the source PAN ID and the source address cannot be selected
    // from the data request parameters, this information is fixed. Therefore, we obtain
    // the values from the present device.
    // See IEEE 802.15.4-2006 , 7.1.1.1
    m_mcpsReqRsp.uParam.sReqData.sFrame.sSrcAddr.u16PanId = s_psMacPib->u16PanId_ReadOnly;

    if (m_mcpsReqRsp.uParam.sReqData.sFrame.sSrcAddr.u8AddrMode == EXT_ADDR)
    {
        uint64_t extAddr = 0;
        uint64_t* extAddrPtr = &extAddr;
        bAHI_ReadMACID(TRUE, extAddrPtr);
        uint32_t extAddrH = (uint32_t)(extAddr >> 32);
        uint32_t extAddrL = (uint32_t)(extAddr);

        m_mcpsReqRsp.uParam.sReqData.sFrame.sSrcAddr.uAddr.sExt.u32H = extAddrH;
        m_mcpsReqRsp.uParam.sReqData.sFrame.sSrcAddr.uAddr.sExt.u32L = extAddrL;
    }
    else
    {
        m_mcpsReqRsp.uParam.sReqData.sFrame.sSrcAddr.uAddr.u16Short =
            s_psMacPib->u16ShortAddr_ReadOnly;
    }
    m_mcpsReqRsp.uParam.sReqData.sFrame.sDstAddr.u8AddrMode = m_rxBuffer[pos++];
    m_mcpsReqRsp.uParam.sReqData.sFrame.sDstAddr.u16PanId = m_rxBuffer[pos++];
    m_mcpsReqRsp.uParam.sReqData.sFrame.sDstAddr.u16PanId |= ((m_rxBuffer[pos++]) << 8);

    if (m_mcpsReqRsp.uParam.sReqData.sFrame.sDstAddr.u8AddrMode == EXT_ADDR)
    {
        m_mcpsReqRsp.uParam.sReqData.sFrame.sDstAddr.uAddr.sExt.u32H = m_rxBuffer[pos++];
        m_mcpsReqRsp.uParam.sReqData.sFrame.sDstAddr.uAddr.sExt.u32H |= ((m_rxBuffer[pos++]) << 8);
        m_mcpsReqRsp.uParam.sReqData.sFrame.sDstAddr.uAddr.sExt.u32H |= ((m_rxBuffer[pos++]) << 16);
        m_mcpsReqRsp.uParam.sReqData.sFrame.sDstAddr.uAddr.sExt.u32H |= ((m_rxBuffer[pos++]) << 24);

        m_mcpsReqRsp.uParam.sReqData.sFrame.sDstAddr.uAddr.sExt.u32L = m_rxBuffer[pos++];
        m_mcpsReqRsp.uParam.sReqData.sFrame.sDstAddr.uAddr.sExt.u32L |= ((m_rxBuffer[pos++]) << 8);
        m_mcpsReqRsp.uParam.sReqData.sFrame.sDstAddr.uAddr.sExt.u32L |= ((m_rxBuffer[pos++]) << 16);
        m_mcpsReqRsp.uParam.sReqData.sFrame.sDstAddr.uAddr.sExt.u32L |= ((m_rxBuffer[pos++]) << 24);
    }
    else if (m_mcpsReqRsp.uParam.sReqData.sFrame.sDstAddr.u8AddrMode == SHORT_ADDR)
    {
        m_mcpsReqRsp.uParam.sReqData.sFrame.sDstAddr.uAddr.u16Short = m_rxBuffer[pos++];
        m_mcpsReqRsp.uParam.sReqData.sFrame.sDstAddr.uAddr.u16Short |= ((m_rxBuffer[pos++]) << 8);
    }
    m_mcpsReqRsp.uParam.sReqData.sFrame.u8TxOptions = m_rxBuffer[pos++];
    // TODO add security options when supported
    m_mcpsReqRsp.uParam.sReqData.sFrame.sSecurityData.u8SecurityLevel = 0;

    m_mcpsReqRsp.uParam.sReqData.sFrame.u8SduLength = m_rxBuffer[pos++];
    uint8_t dataLenght = m_mcpsReqRsp.uParam.sReqData.sFrame.u8SduLength;

    uint8_t i;
    for (i = 0; i < dataLenght; i++)
    {
        m_mcpsReqRsp.uParam.sReqData.sFrame.au8Sdu[i] = m_rxBuffer[pos++];
    }

    vAppApiMcpsRequest(&m_mcpsReqRsp, &m_mcpsSyncCfm);
}

void
OrphanResponse()
{
    uint16_t pos = 0;
    m_mlmeReqRsp.u8ParamLength = sizeof(MAC_MlmeRspOrphan_s);

    m_mlmeReqRsp.uParam.sRspOrphan.sOrphanAddr.u32L = m_rxBuffer[pos++];
    m_mlmeReqRsp.uParam.sRspOrphan.sOrphanAddr.u32L |= ((m_rxBuffer[pos++]) << 8);
    m_mlmeReqRsp.uParam.sRspOrphan.sOrphanAddr.u32L |= ((m_rxBuffer[pos++]) << 16);
    m_mlmeReqRsp.uParam.sRspOrphan.sOrphanAddr.u32L |= ((m_rxBuffer[pos++]) << 24);

    m_mlmeReqRsp.uParam.sRspOrphan.sOrphanAddr.u32H = m_rxBuffer[pos++];
    m_mlmeReqRsp.uParam.sRspOrphan.sOrphanAddr.u32H |= ((m_rxBuffer[pos++]) << 8);
    m_mlmeReqRsp.uParam.sRspOrphan.sOrphanAddr.u32H |= ((m_rxBuffer[pos++]) << 16);
    m_mlmeReqRsp.uParam.sRspOrphan.sOrphanAddr.u32H |= ((m_rxBuffer[pos++]) << 24);

    m_mlmeReqRsp.uParam.sRspOrphan.u16OrphanShortAddr = m_rxBuffer[pos++];
    m_mlmeReqRsp.uParam.sRspOrphan.u16OrphanShortAddr |= ((m_rxBuffer[pos++]) << 8);

    m_mlmeReqRsp.uParam.sRspAssociate.u8Status = m_rxBuffer[pos++];

    // TODO: Allow changing this value when security is supported.
    m_mlmeReqRsp.uParam.sRspOrphan.u8SecurityEnable = FALSE;

    vAppApiMlmeRequest(&m_mlmeReqRsp, &m_mlmeSyncCfm);
}

void
ProcessRxData()
{
    if (m_primitiveGroup == MLME_PRIMITIVE)
    {
        switch (m_mlmeReqRsp.u8Type)
        {
        case MAC_MLME_REQ_ASSOCIATE:
            AssociateRequest();
            break;
        case MAC_MLME_REQ_DISASSOCIATE:
            // TODO
            break;
        case MAC_MLME_REQ_GET:
            GetRequest();
            break;
        case MAC_MLME_REQ_GTS:
            // TODO
            break;
        case MAC_MLME_REQ_RESET:
            // TODO
            break;
        case MAC_MLME_REQ_RX_ENABLE:
            // TODO
            break;
        case MAC_MLME_REQ_SCAN:
            ScanRequest();
            break;
        case MAC_MLME_REQ_SET:
            SetRequest();
            break;
        case MAC_MLME_REQ_START:
            StartRequest();
            break;
        case MAC_MLME_REQ_SYNC:
            // TODO
            break;
        case MAC_MLME_REQ_POLL:
            // TODO
            break;
        case MAC_MLME_RSP_ASSOCIATE:
            AssociateResponse();
            break;
        case MAC_MLME_RSP_ORPHAN:
            OrphanResponse();
            break;
        case MAC_MLME_REQ_VS_EXTADDR:
            // TODO
            break;
        }
    }
    else if (m_primitiveGroup == MCPS_PRIMITIVE)
    {
        if (m_mcpsReqRsp.u8Type == MAC_MCPS_REQ_DATA)
        {
            DataRequest();
        }
    }
}

void
ReceiveByte()
{
    // Read one byte at time
    uint8_t rxByte = u8AHI_UartReadData(DBG_E_UART_0);

    switch (m_state)
    {
    case START:
        if (rxByte == 0xAA)
        {
            m_state = PRIMITIVE_TYPE1;
            m_primitiveGroup = MLME_PRIMITIVE;
            // DBG_vPrintf(TRUE,"In start state\n");
        }
        else if (rxByte == 0xBB)
        {
            m_state = PRIMITIVE_TYPE2;
            m_primitiveGroup = MCPS_PRIMITIVE;
        }
        break;
    case PRIMITIVE_TYPE1:
        // For MLME primitives
        m_mlmeReqRsp.u8Type = rxByte;
        m_state = PARAMETERS_SIZE;
        break;
    case PRIMITIVE_TYPE2:
        // For MCPS primitives
        m_mcpsReqRsp.u8Type = rxByte;
        m_state = PARAMETERS_SIZE;
        break;
    case PARAMETERS_SIZE:
        m_primitiveMaxSize = rxByte;
        m_state = WAIT_DATA;
        break;
    case WAIT_DATA:
        m_rxBuffer[m_rxByteCount] = rxByte;
        m_rxByteCount++;
        if (m_rxByteCount == m_primitiveMaxSize)
        {
            // DBG_vPrintf(TRUE,"data collected %d \n",m_mlmeReqRsp.u8Type);
            // GetExtAddress();
            ProcessRxData();
            m_state = START;
            m_primitiveGroup = NONE;
            m_rxByteCount = 0;
            memset(m_rxBuffer, 0, sizeof(m_rxBuffer));
        }
        break;
    }
}

/*
void
GetExtAddress()
{
    // See JN-AN-1003 (JN51xx Boot Loader Operation)
    // Section: JN516x/7x Index Sector
    //uint8_t extMac[8];
    //memset(extMac,0, 8);
    ///memcpy(extMac, (uint8 *)(0x01001580), 8);
    //SendBytes(extMac, 8);
    //DBG_vPrintf(TRUE,"--> The ext address %x:%x:%x:%x:%x:%x:%x:%x\n",
         // extMac[0],extMac[1],extMac[2],extMac[3],extMac[4],extMac[5],extMac[6],extMac[7]);
    uint64_t extAddr = 0;
    uint8_t pos = 0;
    uint8_t extendedAddress[8];
    uint64_t *extAddrPtr = &extAddr;
    bAHI_ReadMACID(TRUE, extAddrPtr);

    extendedAddress[pos++] = (extAddr & 0xFF);
    extendedAddress[pos++] = (extAddr >> 8) & 0xFF;
    extendedAddress[pos++] = (extAddr >> 16) & 0xFF;
    extendedAddress[pos++] = (extAddr >> 24) & 0xFF;

    extendedAddress[pos++] = (extAddr >> 32) & 0xFF;
    extendedAddress[pos++] = (extAddr >> 40) & 0xFF;
    extendedAddress[pos++] = (extAddr >> 48) & 0xFF;
    extendedAddress[pos++] = (extAddr >> 56) & 0xFF;

    //DBG_vPrintf(TRUE,"--> The ext address %d %x\n",extAddr,extAddr);
    SendBytes(extendedAddress, 8);
}
*/

void
SendBytes(uint8_t bytes[], uint8_t size)
{
    int i = 0;
    for (i = 0; i < size; i++)
    {
        vAHI_UartWriteData(DBG_E_UART_0, bytes[i]);
        // Check that the TX buffers are empty
        while ((u8AHI_UartReadLineStatus(DBG_E_UART_0) & E_AHI_UART_LS_THRE) == 0)
            ;
        while ((u8AHI_UartReadLineStatus(DBG_E_UART_0) & E_AHI_UART_LS_TEMT) == 0)
            ;
    }
}
