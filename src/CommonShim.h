/*
 * Copyright (c) 2024 Tokushima University, Japan.
 *
 * SPDX-License-Identifier: GPL-2.0-only
 *
 * Author:
 *  Alberto Gallegos Ramonet <alramonet@is.tokushima-u.ac.jp>
 */

#include <AppHardwareApi.h>
#include <AppQueueApi.h>
#include <FlashIndexSectorApi.h>
#include <dbg.h>
#include <dbg_jtag.h>
#include <dbg_uart.h>
#include <jendefs.h>
#include <mac_pib.h>
#include <mac_sap.h>
#include <stdlib.h>
#include <string.h>

#ifndef COMMONSHIM_H_
#define COMMONSHIM_H_

#define MAX_PACKET_SIZE 256

typedef enum
{
    E_STATE_IDLE,
    E_STATE_ACTIVE_SCANNING,
    E_STATE_ASSOCIATING,
    E_STATE_ASSOCIATED,
    E_STATE_SYNCBEACON
} teState;

typedef struct
{
    teState eState;
    uint8 u8Channel;
    uint8 u8TxPacketSeqNb;
    uint8 u8RxPacketSeqNb;
    uint16 u16Address;
} tsEndDeviceData;

typedef enum
{
    START,
    PRIMITIVE_TYPE1,
    PRIMITIVE_TYPE2,
    PARAMETERS_SIZE,
    WAIT_DATA,
} ReceiveState;

typedef enum
{
    SCAN_CFM = 1,
    START_CFM = 2,
    ASSOCIATE_CFM = 3,
    ASSOCIATE_IND = 4,
    COMM_STATUS_IND = 5,
    DATA_CFM = 6,
    DATA_IND = 7,
    SET_CFM = 8,
    GET_CFM = 9,
    ORPHAN_IND = 10,
    BEACON_NOTIFY_IND = 11
} PrimitiveType;

typedef enum
{
    SHORT_ADDR = 2,
    EXT_ADDR = 3
} AddressMode;

typedef enum
{
    NONE,
    MLME_PRIMITIVE,
    MCPS_PRIMITIVE
} PrimitiveGroup;

/* Handles from the MAC */
PRIVATE void* s_pvMac;
PRIVATE MAC_Pib_s* s_psMacPib;
PRIVATE tsEndDeviceData sEndDeviceData;

MAC_MlmeReqRsp_s m_mlmeReqRsp;
MAC_MlmeSyncCfm_s m_mlmeSyncCfm;

MAC_McpsReqRsp_s m_mcpsReqRsp;
MAC_McpsSyncCfm_s m_mcpsSyncCfm;

ReceiveState m_state;
PrimitiveGroup m_primitiveGroup;
uint8_t m_rxBuffer[MAX_PACKET_SIZE]; //!< Represents the reception buffer of a single packet
uint32_t m_rxByteCount;
uint16_t m_primitiveMaxSize;

/**
 * Entry point for application from boot loader. Simply jumps to AppColdStart
 * as, in this instance, application will never warm start.
 *
 */
PUBLIC void AppWarmStart();
/**
 * Entry point for application from boot loader. Initialises the system, sets
 * the UART configurations and runs the main loop.
 *
 */
PUBLIC void AppColdStart();

/**
 * Setup the MAC interface, initialize Pib and MAC handles. Initialize the MAC attributes
 * with some default values.
 */
PRIVATE void InitializeSystem();

/**
 * Process incoming MCPS primitive events
 *
 * @param psAHI_Ind The pointer to the callback of the MCPS primitive event received.
 */
PRIVATE void ProcessIncomingMcps(MAC_McpsDcfmInd_s* psMcpsInd);

/**
 * Process incoming MLME primitive events
 *
 * @param psAHI_Ind The pointer to the callback of the MLME primitive event received.
 */
PRIVATE void ProcessIncomingMlme(MAC_MlmeDcfmInd_s* psMlmeInd);

/**
 * Process incoming hardware events
 *
 * @param psAHI_Ind The pointer to the callback of the hardware event received.
 */
PRIVATE void ProcessIncomingHwEvent(AppQApiHwInd_s* psAHI_Ind);

/**
 * Check for incoming queue events (MCPS, MLME, hardware)
 * and process the events accordingly.
 */
PRIVATE void ProcessEventQueues();

/**
 * Process the reception of a single byte via UART.
 */
PRIVATE void ReceiveByte();

/**
 * Process accumulated incoming bytes via UART that represent the necessary parameters to
 * trigger a primitive event.
 */
PRIVATE void ProcessRxData();

/***
 * Send a series of bytes via UART that represent a CONFIRM or INDICATION primitive
 * as a result of a previous issued request.
 *
 * @param bytes A series of bytes representing the parameters
 *        of a confirmation or indication primitive.
 * @param size The number of bytes to be transmitted via UART.
 */
PRIVATE void SendBytes(uint8_t bytes[], uint8_t size);

///////////////////////////////////////
// Request and Response Primitives   //
///////////////////////////////////////

/**
 * Triggers the execution of a MLME-ASSOCIATE.request primitive.
 * Parameters used in this primitive are used from the Rx buffer.
 */
PRIVATE void AssociateRequest();

/**
 * Triggers the excution of a MLME-Get.request primitive.
 * Pib Attributes values can be obtained by directly reading their values from the
 * Mac handle (A Mac_Pib_s object that contains the MAC handle)
 */
PRIVATE void GetRequest();

/**
 * Triggers the execution of a MLME-SCAN.request primitive.
 * Parameters used in this primitive are used from the Rx buffer.
 */
PRIVATE void ScanRequest();

/**
 * Triggers the excution of a MLME-SET.request primitive.
 * In JN516x Some attributes require the use of special functions to set the values because
 * they involve changes in the registers of the MCU. Other values can be simply change
 * by obtaining the MAC handle (A Mac_Pib_s object that contains the MAC handle) and
 * changing the value of the attribute directly.
 */
PRIVATE void SetRequest();

/**
 * Triggers the execution of a MLME-START.request primitive.
 * Parameters used in this primitive are used from the Rx buffer.
 */
PRIVATE void StartRequest();

/**
 * Triggers the execution of a MLME-ASSOCIATE.response primitive.
 * Parameters used in this primitive are used from the Rx buffer.
 */
PRIVATE void AssociateResponse();

/**
 * Triggers the execution of a MCPS-DATA.request primitive.
 * Parameters used in this primitive are used from the Rx buffer.
 */
PRIVATE void DataRequest();

/**
 * Triggers the execution of a MLME-ORPHAN.response primitive.
 * Parameters used in this primitive are used from the Rx buffer.
 */
PRIVATE void OrphanResponse();

///////////////////////////////////////
// Confirm and Indication Primitives //
///////////////////////////////////////

/**
 * This function is called whenever a MLME-ASSOCIATE.confirm is called from the
 * IEEE 802.15.4 device.
 * @param psMlmeInd The object callback use to receive the confirm/indication params
 */
PRIVATE void AssociateConfirm(MAC_MlmeDcfmInd_s* psMlmeInd);

/**
 * This function is called whenever a MLME-ASSOCIATE.indication is called from the
 * IEEE 802.15.4 device.
 * @param psMlmeInd The object callback use to receive the confirm/indication params
 */
PRIVATE void AssociateIndication(MAC_MlmeDcfmInd_s* psMlmeInd);

/**
 *
 * This function is called whenever a MLME-SYNC-LOSS.indication is called from the
 * IEEE 802.15.4 device. Handles the Synchronization Loss Indication when
 * attempting to track beacons.
 *
 * @param psMlmeInd The object callback use to receive the confirm/indication params
 */
PRIVATE void SyncLossIndication(MAC_MlmeDcfmInd_s* psMlmeInd);

/**
 * This function is called whenever a MLME-COMMUNICATION-STATUS.indication is called from the
 * IEEE 802.15.4 device.
 * @param psMlmeInd The object callback use to receive the confirm/indication params
 */
PRIVATE void CommStatusIndication(MAC_MlmeDcfmInd_s* psMlmeInd);

/**
 * This function is called whenever a MLME-SCAN.confirm is called from the
 * IEEE 802.15.4 device.
 * @param psMlmeInd The object callback use to receive the confirm/indication params
 */
PRIVATE void ScanConfirm(MAC_MlmeDcfmInd_s* psMlmeInd);

/**
 * This function is called whenever a MLME-ORPHAN.indication is called from the
 * IEEE 802.15.4 device.
 * @param psMlmeInd The object callback use to receive the confirm/indication params
 */
PRIVATE void OrphanIndication(MAC_MlmeDcfmInd_s* psMlmeInd);

/**
 * This function is called whenever a MLME-BEACON-NOTIFY.indication is called from the
 * IEEE 802.15.4 device. This happens when a beacon containing payload is received.
 *
 * @param psMlmeInd The object callback use to receive the confirm/indication params
 */
PRIVATE void BeaconNotifyIndication(MAC_MlmeDcfmInd_s* psMlmeInd);

#endif /* COMMONSHIM_H_ */
