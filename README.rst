

Common Shim layer for JN5169
============================

This project creates a common shim layer to directly interface with an IEEE 802.15.4-2006 MAC layer of a
NXP JN5169 device via UART. This gives co-processor capabilities to the JN5169.

The original purpose of this project is to allow lr-wpan (IEEE 802.15.4) applications and protocols created on the ns-3 network simulator interface with real hardware.
However, it can also be used to test or develop IoT protocols or interface with other applications developed
in c++, python, etc which reside outside the NXP MCU.::

    +-------------------------------+
    | L3~L7: Protocol & Application |       Handled by the Host processor (e.g. ns-3 protocol stack)
    +-------------------------------+
             ^
             |  UART
             |
    +-------------------------------+
    |        Common Shim layer      |
    +-------------------------------+
    |  L2: IEEE 802.15.4-2006 MAC   |       Handled by the NXP MCU processor (JN5169 device)
    +-------------------------------+
    |  L1: IEEE 802.15.4-2006 PHY   |
    +-------------------------------+

Cross compiler
--------------

This project requires the cross compiler ba-elf-gcc (GCC) 4.7.4 to generate the bin file that
contains the the MAC layer and our common shim layer for the JN5169.

This cross compiler can be found around but if you need one site where you can get it
try the following site from the Japanese company mono-wireless:

- https://mono-wireless.com/jp/products/stage/index.html

Look for the linux zip file (TWELITE STAGE SDK). Inside this zip, look for the tool folder and
extract the folder ba-elf-ba2-r36379. This folder contains the gcc cross compiler used to created embedded applications for
NXP JN51xx devices.

Drivers
-------

The JN51xx devices feature FTDI serial-to-USB modules.
The driver is commonly found in most OS, but if required it can be downloaded from http://www.ftdichip.com/Drivers/VCP.htm


Other requirements
------------------

NXP 's `JN-SW-4163 <https://www.nxp.com/products/wireless-connectivity/zigbee/ieee-802-15-4-for-jn516x-7x:IEEE802.15.4>`_  SDK (Obtained from Nxp website).

This SDK contains the dynamic libraries and files that represent the IEEE 802.15.4-2006 PHY and MAC layer for
JN516x-JN517x devices.

Additionally to this SDK, several headers `FlashIndexSectorApi_JN51xx.h` have been added to the SDK's
`/Components/HardwareApi/Include` folder. These headers are not distributed with the SDK, but they were provided by
NXP upon request. These headers are used to access the device OTP (One Time Program) and index sectors of the MCU. This is necessary to obtain useful
information from the flash user space such as the device `MAC extended address`.

The `JN-SW-4163` SDK  is included in the current project but can also be downloaded direclty from the NXP's website. However, please be aware that the SDK on the NXP website do not contain the headers metioned above or
our custom CMakeLists.txt file. This CMakeList.txt file eliminates the dependency on Windows platforms.
NXP only provide support and instructions for Windows and force you to use its "Beyond Studio" IDE. But as shown in this project, it is possible develop and compile
programs for JN516x using any `ba-elf-ba2 compiler` , the correct toolchain configuration and the IDE and OS of your choice.

By default, the CMakeList file will look for the the compiler at `/usr/ba-elf-gcc`.
Modify this line and/or add enviromental variables to allow CMAKE to find the cross compiler.


Compiling the Project
---------------------

The project main code is contained in the `src/` folder under the name `CommonShim.c` and the header `CommonShim.h`.


To compile, go to the root directory of the project and run cmake to start configuration of the project::

 $>cmake -DCMAKE_TOOLCHAIN_FILE=../ba-elf-toolchain.cmake ..

Followed by the command "make". This should generate the required `CommonShim_JN5169.bin` in the `build\` folder.


Alternatively, if you use the vscode IDE with the `CMake` and `CMake Tools` extensions installed you simply need to
open the folder (which will prompt CMake to run the CMakeList.txt configurations automatically) and press the button
"Build".

Target devices
--------------

The following is a list of the tested devices with the common shim.

.. image:: images/monostick.jpg
   :width: 500px
   :height: 358px
   :alt: Monostick (USB dongle with JN5169)
   :align: left

Monostick (USB dongle with JN5169)


.. image:: images/om15080.jpg
   :width: 510px
   :height: 285px
   :alt: NXP OM15080 (USB dongle with JN5169)
   :align: left

NXP OM15080 (USB dongle with JN5169)


.. image:: images/tweliteCue.png
   :width: 500px
   :height: 500px
   :alt: Twelite CUE (Coin battery operated module with JN5169)
   :align: left

Twelite CUE (Coin battery operated module with JN5169)


While we only tested with the JN5169 devices listed above, with some small modifications to the CMakeLists.txt you
should be able to create a file compatible with other JN5169 devices/boards or JN516x and JN517x family devices as they
share common code libraries.

Programming the JN5169 device
-----------------------------

Once the bin file is generated you can flash your JN5169 device using one of many flashers and programmers.

JN51xx Production Flash Programmer
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The first obvious option is the `JN-SW-4107 <https://www.nxp.com/products/wireless-connectivity/zigbee/support-resources-for-jn516x-mcus:SUPPORT-RESOURCES-JN516X-MCUSr>`_ , also known as the `JN51xx Production Flash Programmer`.
This programmer is provided by NXP but it's only available for Windows. Regardless, it is a good option
to program a chip in the JN51xx MCU family. This also includes the develpment boards supported by NXP.
This programmer, ocationally will not be able to flash JN516x devices that are created by other companies (e.g. https://mono-wireless.com/en/).
In our tests, It is able to flash both the NXP OM15080 and Monostick, but it is unable to flash the Twelite CUE.

For further usage details check the `JN-UG-3099 <https://www.nxp.com/products/wireless-connectivity/zigbee/ieee-802-15-4-for-jn516x-7x:IEEE802.15.4>`_ manual on the NXP website.


TweTerm
~~~~~~~

This a test console developed by the Japanese company mono-wireless that includes programming capabilities. From our tested programmers
this was the only programmer that was platform independent (Runs on Windows, Linux and MacOs) and capable of flashing
our target devices both from NXP and from mono-wireless.

To list the available devices (connected devices) to flash run the following command:::

    sudo ./tweterm.py -p "ftdi:///?"

To program the device input the following command: ::

    sudo ./tweterm.py -p "<ftdi device>" -b <byte rate> -F <bin file>

Example:::

    sudo ./tweterm.py -p "ftdi://ftdi:232:MW3V5XLR/1" -b 115200 -F CommonShim_JN5169.bin

After programming the device you can exit the programmer by pressing Ctrl + c followed by pressing "x".

Modified JennicModuleProgrammer
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The JennicModule programmer was officially developed by NXP (for windows platforms) to program Microcontrollers such as JN5148, JN5142, JN5139.
`Modified versions <https://github.com/alejandrolozano2/JN5169-Programmer>`_ that supports JN5169 created by the community can be found around.

These modified JennicModuleProgrammers run only on Windows and therefore, they were not tested in this project but they can be usuful to others.


JN51xx flasher
~~~~~~~~~~~~~~

The `JN51xx flasher <https://github.com/grafalex82/jn51xx_flasher>`_ was written by Oleksandr Masliuchenko and it is platform independent but it only flashes some JN5169 devices. A detailed development documentation
can be found about this flasher, and it provides a good starting point for future development or improvents.

In our test, the programmer was not able to program our target devices, in essence, JN5169 chip that are deployed on a USB dongle.
Other non USB dongle devices seems to work with the programmer.








